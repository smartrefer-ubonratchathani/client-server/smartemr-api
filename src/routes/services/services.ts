import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { StatusCodes } from "http-status-codes";
import { AxiosResponse } from "axios";
import * as jwt from 'jsonwebtoken';
import { ServiceModels } from "../../models/service-models/smr_datas-model";
import moment from 'moment-timezone';
var cron = require('node-cron');
const minute = Math.floor(Math.random() * 59);
const hour = Math.floor(Math.random() * 8);
const delayInterval = 80000; // set delay interval 200 milisecond 200000
const delayInterval2 = 10000; // set delay interval 200 milisecond 200000



// model
import { HisUniversalHiModel } from '../../models/his_universal.model';
import { HisHiModel } from '../../models/his_hi.model';
import { HisHomcModel } from '../../models/his_homc.model';
import { HisHosxpv3Model } from '../../models/his_hosxpv3.model';
import { HisHosxpv3ArjaroModel } from '../../models/his_hosxpv3arjaro.model';
import { HisHosxpv4Model } from '../../models/his_hosxpv4.model';
import { HisHosxpv4PgModel } from '../../models/his_hosxpv4pg.model';
import { HisHosxpv4PCUModel } from '../../models/his_hosxpv4_pcu.model';
import { HisMbaseModel } from '../../models/his_mbase.model';
import { HisPhospModel } from '../../models/his_phosp.model';
import { HisHimproHiModel } from '../../models/his_himpro.model';
import { HisJhcisHiModel } from '../../models/his_jhcis.model';
import { HisUbaseModel } from '../../models/his_ubase.model';
import { HisSakonModel } from '../../models/his_sakon.model';
import { HisHomcUbonModel } from '../../models/his_homc_udon.model';
import { HisSoftConModel } from '../../models/his_softcon.model'
import { HisSoftConUdonModel } from '../../models/his_softcon_udon.model'
import { HisSsbModel } from '../../models/his_ssb.model'
import { HisPanaceaplusModel } from '../../models/his_panaceaplus.model'
import { result } from 'lodash';

export default async (fastify: FastifyInstance, _options: any, done: any) => {
    let Today_ = Date.now();
    let x_ = Today_ - (1 * 24 * 60 * 60 * 1000);
    let yesterday_ = moment(x_).tz('Asia/Bangkok').format('YYYY-MM-DD')
    console.log(`Test running a task every minute: ${minute} / hour: ${hour}`);
    console.log('Test YesterDay :', moment(yesterday_).tz('Asia/Bangkok').format('YYYY-MM-DD'));

    // ห้ามแก้ไข // 
    //-----------------BEGIN START-------------------//
    const provider = process.env.API_HIS_PROVIDER;
    const db = fastify.db;
    const serviceModels = new ServiceModels();
    let hisModel: any;

    switch (provider) {
        case 'ubase':
            hisModel = new HisUbaseModel();
            break;
        case 'himpro':
            hisModel = new HisHimproHiModel();
            break;
        case 'jhcis':
            hisModel = new HisJhcisHiModel();
            break;
        case 'hosxpv3':
            hisModel = new HisHosxpv3Model();
            break;
        case 'hosxpv3arjaro':
            hisModel = new HisHosxpv3ArjaroModel();
            break;
        case 'hosxpv4':
            hisModel = new HisHosxpv4Model();
            break;
        case 'hosxpv4pg':
            hisModel = new HisHosxpv4PgModel();
            break;
        case 'hi':
            hisModel = new HisHiModel();
            break;
        case 'homc':
            hisModel = new HisHomcModel();
            break;
        case 'mbase':
            hisModel = new HisMbaseModel();
            break;
        case 'phosp':
            hisModel = new HisPhospModel();
            break;
        case 'universal':
            hisModel = new HisUniversalHiModel();
            break;
        case 'sakon':
            hisModel = new HisSakonModel();
            break;
        case 'homcudon':
            hisModel = new HisHomcUbonModel();
            break;
        case 'softcon':
            hisModel = new HisSoftConModel();
            break;
        case 'softconudon':
            hisModel = new HisSoftConUdonModel();
            break;
        case 'ssb':
            hisModel = new HisSsbModel();
            break;
        case 'panaceaplus':
            hisModel = new HisPanaceaplusModel();
            break;
        case 'hosxppcuv4':
            hisModel = new HisHosxpv4PCUModel();
            break;
        default:
        // hisModel = new HisModel();
    }
    //------------------BEGIN END------------------//
    // view/00000/00000000/64000000
    fastify.get('/view/:hn/:seq', {
        preHandler: [
            //   fastify.guard.role(['admin','nurse','doctor'])
            // fastify.guard.scope(['nurse.ward','nurse.create'])
        ],
        // schema: listSchema,
    }, async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        let hn: any = req.params.hn;
        let seq: any = req.params.seq;
        let objService: any = {};
        let providerCode: any;
        let providerName: any;
        let profile: any = [];
        if (hn && seq) {
            try {
                let rs_hospital = await hisModel.getHospital(db, hn);
                // console.log("rs_hospital:", rs_hospital);

                if (rs_hospital[0]) {
                    providerCode = rs_hospital[0].provider_code;
                    providerName = rs_hospital[0].provider_name;
                }
                let rs_profile = await hisModel.getProfile(db, hn, seq);
                // console.log("rs_profile:", rs_profile);
                if (rs_profile[0]) {
                    for (const rpro of rs_profile) {
                        let brthdate: any = moment(rpro.brthdate).tz('Asia/Bangkok').format('YYYY-MM-DD');
                        if (brthdate == 'Invalid date') { brthdate = null }
                        let objPro = {
                            "hospcode": providerCode,
                            "hospname": providerName,
                            "hn": rpro.hn,
                            "cid": rpro.cid,
                            "title_name": rpro.title_name,
                            "first_name": rpro.first_name,
                            "last_name": rpro.last_name,
                            "moopart": rpro.moopart,
                            "addrpart": rpro.addrpart,
                            "tmbpart": rpro.tmbpart,
                            "amppart": rpro.amppart,
                            "chwpart": rpro.chwpart,
                            "brthdate": brthdate,
                            "age": rpro.age,
                            "sex": rpro.sex,
                            "occupation": rpro.occupation,
                            "pttype_id": rpro.pttype_id,
                            "pttype_name": rpro.pttype_name,
                            "pttype_no": rpro.pttype_no,
                            "hospmain": rpro.hospmain,
                            "hospmain_name": rpro.hospmain_name,
                            "hospsub": rpro.hospsub,
                            "hospsub_name": rpro.hospsub_name,
                            "father_name": rpro.father_name,
                            "mother_name": rpro.mother_name,
                            "couple_name": rpro.couple_name,
                            "contact_name": rpro.contact_name,
                            "contact_relation": rpro.contact_relation,
                            "contact_mobile": rpro.contact_mobile,
                            "registdate": moment(rpro.registdate).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                            "visitdate": moment(rpro.visitdate).tz('Asia/Bangkok').format('YYYY-MM-DD')
                        }
                        profile.push(objPro);
                    }
                    objService.profile = profile;
                }

                let rs_allergy = await hisModel.getAllergyDetail(db, hn);
                // console.log("rs_allergy:", rs_allergy[0]);
                if (rs_allergy[0]) {
                    let allergy: any = [];
                    for (const ra of rs_allergy) {
                        let objAllergy = {
                            "hospcode": providerCode,
                            "hospname": providerName,
                            "drug_name": ra.drug_name,
                            "symptom": ra.symptom,
                            "begin_date": moment(ra.begin_date).tz('Asia/Bangkok').format('YYYY-MM-DD'),    //วันที่แพ้ยา
                            "daterecord": ra.daterecord     //วันที่บันทึกประวัติแพ้ยา
                        }
                        allergy.push(objAllergy);
                    }
                    objService.allergy = allergy;
                }

                let rs_services = await hisModel.getServices(db, hn, seq);
                // console.log("rs_services:", rs_services[0]);
                if (rs_services[0]) {
                    let pillness: any = [];
                    let physicals: any = [];
                    let nurtures: any = [];
                    let diagnosis: any = [];
                    let drugs: any = [];
                    let lab: any = [];
                    let procedure: any = [];
                    let xray: any = [];
                    let visit: any = [];
                    for (const v of rs_services) {
                        let objVisit = {
                            "hospcode": providerCode,
                            "hospname": providerName,
                            "seq": seq,
                            "title_name": v.title_name,
                            "first_name": v.first_name,
                            "last_name": v.last_name,
                            "date_serv": v.date_serv,
                            "time_serv": v.time_serv,
                            "department": v.department,
                        }
                        visit.push(objVisit);
                        objService.visit = visit;

                        let rs_pillness = await hisModel.getPillness(db, hn, v.date_serv, seq);
                        // console.log("rs_pillness:", rs_pillness[0]);
                        if (rs_pillness[0]) {
                            for (const ri of rs_pillness) {
                                let objPillness = {
                                    "hospcode": providerCode,
                                    "hospname": providerName,
                                    "seq": ri.seq,
                                    "pi": ri.hpi,
                                }
                                pillness.push(objPillness);
                            }
                            objService.pillness = pillness;
                        } else {
                            objService.pillness = [];
                        }

                        let rs_physicals = await hisModel.getPhysical(db, hn, v.date_serv, seq);
                        // console.log("rs_nurtures:", rs_physicals[0]);
                        if (rs_physicals[0]) {
                            for (const rp of rs_physicals) {
                                let objPhysicals = {
                                    "hospcode": providerCode,
                                    "hospname": providerName,
                                    "seq": rp.seq,
                                    "pe": rp.pe,
                                }
                                physicals.push(objPhysicals);
                            }
                            objService.physicals = physicals;
                        } else {
                            objService.physicals = [];
                        }

                        let rs_nurtures = await hisModel.getNurture(db, hn, v.date_serv, seq);
                        // console.log("rs_nurtures:", rs_nurtures[0]);
                        if (rs_nurtures[0]) {
                            for (const rn of rs_nurtures) {
                                let objNurtures = {
                                    "hospcode": providerCode,
                                    "hospname": providerName,
                                    "seq": rn.seq,
                                    "date_serv": moment(rn.date_serv).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                    "time_serv": rn.time_serv,
                                    "bloodgrp": rn.bloodgrp,
                                    "weight": rn.weight || 0,
                                    "height": rn.height || 0,
                                    "bmi": rn.bmi || 0,
                                    "temperature": rn.temperature,
                                    "pr": rn.pr,
                                    "rr": rn.rr,
                                    "sbp": rn.sbp,
                                    "dbp": rn.dbp,
                                    "symptom": rn.symptom,
                                    "pmh": rn.pmh,
                                    "depcode": rn.depcode,
                                    "department": rn.department,
                                    "movement_score": rn.movement_score,
                                    "vocal_score": rn.vocal_score,
                                    "eye_score": rn.eye_score,
                                    "oxygen_sat": rn.oxygen_sat,
                                    "gak_coma_sco": rn.gak_coma_sco,
                                    "pupil_right": rn.pupil_right,
                                    "pupil_left": rn.pupil_left,
                                    "diag_text": rn.diag_text || ''
                                }
                                nurtures.push(objNurtures);
                            }
                            objService.nurtures = nurtures;
                        } else {
                            objService.nurtures = [];
                        }

                        let rs_diagnosis = await hisModel.getDiagnosis(db, hn, v.date_serv, seq);
                        // console.log("rs_diagnosis:", rs_diagnosis[0]);
                        if (rs_diagnosis[0]) {
                            for (const rg of rs_diagnosis) {
                                let objDiagnosis = {
                                    "hospcode": providerCode,
                                    "hospname": providerName,
                                    "seq": rg.seq,
                                    "date_serv": moment(rg.date_serv).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                    "time_serv": rg.time_serv,
                                    "icd_code": rg.icd_code,
                                    "icd_name": rg.icd_name,
                                    "diag_type": rg.diag_type,
                                    "diag_note": rg.DiagNote,        //DiagNote
                                    "diagtype_id": rg.diagtype_id   //diagtype_id
                                }
                                diagnosis.push(objDiagnosis);
                            }
                            objService.diagnosis = diagnosis;
                        } else {
                            objService.diagnosis = [];
                        }

                        let rs_procedure = await hisModel.getProcedure(db, hn, v.date_serv, seq);
                        // console.log("rs_procedure:", rs_procedure[0]);
                        if (rs_procedure[0]) {
                            for (const rp of rs_procedure) {
                                let objProcedure = {
                                    "hospcode": providerCode,
                                    "hospname": providerName,
                                    "seq": rp.seq,
                                    "date_serv": moment(rp.date_serv).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                    "time_serv": rp.time_serv,
                                    "procedure_code": rp.procedure_code,
                                    "procedure_name": rp.procedure_name,
                                    "start_date": moment(rp.start_date).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                    "start_time": rp.start_time,
                                    "end_date": rp.end_date ? moment(rp.end_date).tz('Asia/Bangkok').format('YYYY-MM-DD') : rp.end_date,
                                    "end_time": rp.end_time
                                }
                                procedure.push(objProcedure);
                            }
                            objService.procedure = procedure;
                        } else {
                            objService.procedure = [];
                        }

                        let rs_drugs = await hisModel.getDrugs(db, hn, v.date_serv, seq);
                        // console.log("rs_drugs:", rs_drugs[0]);
                        if (rs_drugs[0]) {
                            for (const rd of rs_drugs) {
                                let objDrug = {
                                    "hospcode": providerCode,
                                    "hospname": providerName,
                                    "seq": rd.seq,
                                    "date_serv": moment(rd.date_serv).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                    "time_serv": rd.time_serv,
                                    "drug_name": rd.drug_name,
                                    "qty": rd.qty,
                                    "unit": rd.unit,
                                    "usage_line1": rd.usage_line1,
                                    "usage_line2": rd.usage_line2 || null,
                                    "usage_line3": rd.usage_line3 || null,
                                }
                                drugs.push(objDrug);
                            }
                            objService.drugs = drugs;
                        } else {
                            objService.drugs = [];
                        }

                        let rs_lab = await hisModel.getLabs(db, hn, v.date_serv, seq);
                        // console.log("rs_lab:", rs_lab[0]);
                        if (rs_lab[0]) {
                            for (const rl of rs_lab) {
                                let standard_result: any;
                                if (rl.standard_result) {
                                    standard_result = rl.standard_result.toString();
                                } else {
                                    standard_result = rl.standard_result;
                                }
                                let objLab = {
                                    "hospcode": providerCode,
                                    "hospname": providerName,
                                    "seq": seq,
                                    "date_serv": moment(rl.date_serv).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                    "time_serv": rl.time_serv,
                                    "labgroup": rl.labgroup,
                                    "lab_name": rl.lab_name,
                                    "lab_result": rl.lab_result,
                                    "unit": rl.unit,
                                    "standard_result": standard_result
                                }
                                lab.push(objLab);
                            }
                            objService.lab = lab;
                        } else {
                            objService.lab = [];
                        }

                        let rs_xray = await hisModel.getXray(db, hn, v.date_serv, seq);
                        // console.log("rs_xray:", rs_xray[0]);
                        if (rs_xray[0]) {
                            for (const v of rs_xray) {
                                let objXray = {
                                    "hospcode": providerCode,
                                    "hospname": providerName,
                                    "seq": seq,
                                    "xray_date": moment(v.xray_date).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                    "xray_name": v.xray_name,
                                }
                                xray.push(objXray);
                            }
                            objService.xray = xray;
                        } else {
                            objService.xray = [];
                        }
                    }
                }

                if (objService) {
                    reply.send(objService);
                } else {
                    reply.send({ ok: false });
                }
            } catch (error: any) {
                console.log(error);
                reply.send({ ok: false, error: error.message });
            }
        } else {
            reply.send({ ok: false, error: 'Incorrect data!' });
        }

    })

    fastify.get('/visit/:startDate/:endDtae', async (request: FastifyRequest, reply: FastifyReply) => {
        let rs_datas_suc: any = [];
        let rs_datas_err: any = [];
        let objService: any = {};
        let rs_error: any = [];

        // try {
            const req: any = request
            let startDate: any = req.params.startDate;
            let endDtae: any = req.params.endDtae;
            let rs_visit = await hisModel.getVisit(db, startDate, endDtae);
            for (let x of rs_visit) {
                const hisSecretKey: any = process.env.API_HIS_SECRET_KEY || '';

                var token: any = jwt.sign({
                    exp: Math.floor(Date.now() / 1000) + (10 * 60), // 2 minute
                }, hisSecretKey);


                let providerCode: any;
                let providerName: any;
                let profile: any = [];
                if (x.hn && x.seq) {
                    // try {
                        let rs_hospital = await hisModel.getHospital(db, x.hn);
                        // console.log("rs_hospital:", rs_hospital);

                        if (rs_hospital[0]) {
                            providerCode = rs_hospital[0].provider_code;
                            providerName = rs_hospital[0].provider_name;
                        }
                        let rs_profile = await hisModel.getProfile(db, x.hn, x.seq);
                        // console.log("rs_profile:", rs_profile);
                        if (rs_profile[0]) {
                            for (const rpro of rs_profile) {

                                let brthdate: any = moment(rpro.brthdate).tz('Asia/Bangkok').format('YYYY-MM-DD')
                                if (brthdate == 'Invalid date') { brthdate = null }
                                let objPro = {
                                    "hospcode": providerCode,
                                    "hospname": providerName,
                                    "hn": rpro.hn,
                                    "cid": rpro.cid,
                                    "title_name": rpro.title_name,
                                    "first_name": rpro.first_name,
                                    "last_name": rpro.last_name,
                                    "moopart": rpro.moopart,
                                    "addrpart": rpro.addrpart,
                                    "tmbpart": rpro.tmbpart,
                                    "amppart": rpro.amppart,
                                    "chwpart": rpro.chwpart,
                                    "brthdate": brthdate,
                                    "age": rpro.age,
                                    "sex": rpro.sex,
                                    "occupation": rpro.occupation,
                                    "pttype_id": rpro.pttype_id,
                                    "pttype_name": rpro.pttype_name,
                                    "pttype_no": rpro.pttype_no,
                                    "hospmain": rpro.hospmain,
                                    "hospmain_name": rpro.hospmain_name,
                                    "hospsub": rpro.hospsub,
                                    "hospsub_name": rpro.hospsub_name,
                                    "father_name": rpro.father_name,
                                    "mother_name": rpro.mother_name,
                                    "couple_name": rpro.couple_name,
                                    "contact_name": rpro.contact_name,
                                    "contact_relation": rpro.contact_relation,
                                    "contact_mobile": rpro.contact_mobile,
                                    "registdate": moment(rpro.registdate).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                    "visitdate": moment(rpro.visitdate).tz('Asia/Bangkok').format('YYYY-MM-DD')
                                }
                                profile.push(objPro);
                            }
                            objService.profile = profile;
                        }

                        let rs_allergy = await hisModel.getAllergyDetail(db, x.hn);
                        // console.log("rs_allergy:", rs_allergy[0]);
                        if (rs_allergy[0]) {
                            let allergy: any = [];
                            for (const ra of rs_allergy) {
                                let objAllergy = {
                                    "hospcode": providerCode,
                                    "hospname": providerName,
                                    "drug_name": ra.drug_name,
                                    "symptom": ra.symptom,
                                    "begin_date": moment(ra.begin_date).tz('Asia/Bangkok').format('YYYY-MM-DD'),    //วันที่แพ้ยา
                                    "daterecord": ra.daterecord     //วันที่บันทึกประวัติแพ้ยา
                                }
                                allergy.push(objAllergy);
                            }
                            objService.allergy = allergy;
                        }

                        let rs_services = await hisModel.getServices(db, x.hn, x.seq);
                        // console.log("rs_services:", rs_services[0]);
                        if (rs_services[0]) {
                            let pillness: any = [];
                            let physicals: any = [];
                            let nurtures: any = [];
                            let diagnosis: any = [];
                            let drugs: any = [];
                            let lab: any = [];
                            let procedure: any = [];
                            let xray: any = [];
                            let visit: any = [];
                            for (const v of rs_services) {
                                let objVisit = {
                                    "hospcode": providerCode,
                                    "hospname": providerName,
                                    "seq": x.seq,
                                    "hn": v.hn,
                                    "an": v.an,
                                    "title_name": v.title_name,
                                    "first_name": v.first_name,
                                    "last_name": v.last_name,
                                    "date_serv": v.date_serv,
                                    "time_serv": v.time_serv,
                                    "department": v.department,
                                }
                                visit.push(objVisit);
                                objService.visit = visit;

                                let rs_pillness = await hisModel.getPillness(db, x.hn, v.date_serv, x.seq);
                                // console.log("rs_pillness:", rs_pillness[0]);
                                if (rs_pillness[0]) {
                                    for (const ri of rs_pillness) {
                                        let objPillness = {
                                            "hospcode": providerCode,
                                            "hospname": providerName,
                                            "seq": ri.seq,
                                            "pi": ri.hpi,
                                        }
                                        pillness.push(objPillness);
                                    }
                                    objService.pillness = pillness;
                                } else {
                                    objService.pillness = [];
                                }

                                let rs_physicals = await hisModel.getPhysical(db, x.hn, v.date_serv, x.seq);
                                // console.log("rs_nurtures:", rs_physicals[0]);
                                if (rs_physicals[0]) {
                                    for (const rp of rs_physicals) {
                                        let objPhysicals = {
                                            "hospcode": providerCode,
                                            "hospname": providerName,
                                            "seq": rp.seq,
                                            "pe": rp.pe,
                                        }
                                        physicals.push(objPhysicals);
                                    }
                                    objService.physicals = physicals;
                                } else {
                                    objService.physicals = [];
                                }

                                let rs_nurtures = await hisModel.getNurture(db, x.hn, v.date_serv, x.seq);
                                // console.log("rs_nurtures:", rs_nurtures[0]);
                                if (rs_nurtures[0]) {
                                    for (const rn of rs_nurtures) {
                                        let objNurtures = {
                                            "hospcode": providerCode,
                                            "hospname": providerName,
                                            "seq": rn.seq,
                                            "date_serv": moment(rn.date_serv).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                            "time_serv": rn.time_serv,
                                            "bloodgrp": rn.bloodgrp,
                                            "weight": rn.weight || 0,
                                            "height": rn.height || 0,
                                            "bmi": rn.bmi || 0,
                                            "temperature": rn.temperature,
                                            "pr": rn.pr,
                                            "rr": rn.rr,
                                            "sbp": rn.sbp,
                                            "dbp": rn.dbp,
                                            "symptom": rn.symptom,
                                            "pmh": rn.pmh,
                                            "depcode": rn.depcode,
                                            "department": rn.department,
                                            "movement_score": rn.movement_score,
                                            "vocal_score": rn.vocal_score,
                                            "eye_score": rn.eye_score,
                                            "oxygen_sat": rn.oxygen_sat,
                                            "gak_coma_sco": rn.gak_coma_sco,
                                            "pupil_right": rn.pupil_right,
                                            "pupil_left": rn.pupil_left,
                                            "diag_text": rn.diag_text || ''
                                        }
                                        nurtures.push(objNurtures);
                                    }
                                    objService.nurtures = nurtures;
                                } else {
                                    objService.nurtures = [];
                                }

                                let rs_diagnosis = await hisModel.getDiagnosis(db, x.hn, v.date_serv, x.seq);
                                // console.log("rs_diagnosis:", rs_diagnosis[0]);
                                if (rs_diagnosis[0]) {
                                    for (const rg of rs_diagnosis) {
                                        let objDiagnosis = {
                                            "hospcode": providerCode,
                                            "hospname": providerName,
                                            "seq": rg.seq,
                                            "date_serv": moment(rg.date_serv).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                            "time_serv": rg.time_serv,
                                            "icd_code": rg.icd_code,
                                            "icd_name": rg.icd_name,
                                            "diag_type": rg.diag_type,
                                            "diag_note": rg.DiagNote,        //DiagNote
                                            "diagtype_id": rg.diagtype_id   //diagtype_id
                                        }
                                        diagnosis.push(objDiagnosis);
                                    }
                                    objService.diagnosis = diagnosis;
                                } else {
                                    objService.diagnosis = [];
                                }

                                let rs_procedure = await hisModel.getProcedure(db, x.hn, v.date_serv, x.seq);
                                // console.log("rs_procedure:", rs_procedure[0]);
                                if (rs_procedure[0]) {
                                    for (const rp of rs_procedure) {
                                        let objProcedure = {
                                            "hospcode": providerCode,
                                            "hospname": providerName,
                                            "seq": rp.seq,
                                            "date_serv": moment(rp.date_serv).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                            "time_serv": rp.time_serv,
                                            "procedure_code": rp.procedure_code,
                                            "procedure_name": rp.procedure_name,
                                            "start_date": moment(rp.start_date).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                            "start_time": rp.start_time,
                                            "end_date": rp.end_date ? moment(rp.end_date).tz('Asia/Bangkok').format('YYYY-MM-DD') : rp.end_date,
                                            "end_time": rp.end_time
                                        }
                                        procedure.push(objProcedure);
                                    }
                                    objService.procedure = procedure;
                                } else {
                                    objService.procedure = [];
                                }

                                let rs_drugs = await hisModel.getDrugs(db, x.hn, v.date_serv, x.seq);
                                // console.log("rs_drugs:", rs_drugs[0]);
                                if (rs_drugs[0]) {
                                    for (const rd of rs_drugs) {
                                        let objDrug = {
                                            "hospcode": providerCode,
                                            "hospname": providerName,
                                            "seq": rd.seq,
                                            "date_serv": moment(rd.date_serv).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                            "time_serv": rd.time_serv,
                                            "drug_name": rd.drug_name,
                                            "qty": rd.qty,
                                            "unit": rd.unit,
                                            "usage_line1": rd.usage_line1,
                                            "usage_line2": rd.usage_line2,
                                            "usage_line3": rd.usage_line3
                                        }
                                        drugs.push(objDrug);
                                    }
                                    objService.drugs = drugs;
                                } else {
                                    objService.drugs = [];
                                }

                                let rs_lab = await hisModel.getLabs(db, x.hn, v.date_serv, x.seq);
                                // console.log("rs_lab:", rs_lab[0]);
                                if (rs_lab[0]) {
                                    for (const rl of rs_lab) {
                                        let standard_result: any;
                                        if (rl.standard_result) {
                                            standard_result = rl.standard_result.toString();
                                        } else {
                                            standard_result = rl.standard_result;
                                        }
                                        let date_serv: any = moment(rl.date_serv).tz('Asia/Bangkok').format('YYYY-MM-DD');
                                        if (date_serv == 'Invalid date') { date_serv = v.date_serv }
                                        let objLab = {
                                            "hospcode": providerCode,
                                            "hospname": providerName,
                                            "seq": x.seq,
                                            "date_serv": date_serv,
                                            "time_serv": rl.time_serv,
                                            "labgroup": rl.labgroup,
                                            "lab_name": rl.lab_name,
                                            "lab_result": rl.lab_result,
                                            "unit": rl.unit || null,
                                            "standard_result": standard_result || null,
                                        }
                                        lab.push(objLab);
                                    }
                                    objService.lab = lab;
                                } else {
                                    objService.lab = [];
                                }

                                let rs_xray = await hisModel.getXray(db, x.hn, v.date_serv, x.seq);
                                // console.log("rs_xray:", rs_xray[0]);
                                if (rs_xray[0]) {
                                    for (const v of rs_xray) {
                                        let objXray = {
                                            "hospcode": providerCode,
                                            "hospname": providerName,
                                            "seq": x.seq,
                                            "xray_date": moment(v.xray_date).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                            "xray_name": v.xray_name,
                                        }
                                        xray.push(objXray);
                                    }
                                    objService.xray = xray;
                                } else {
                                    objService.xray = [];
                                }
                            }
                        }

                        if (objService) {
                            const response: AxiosResponse = await serviceModels.getImportService(fastify.axios, token, objService);
                            // rs_datas_suc.push(objService);
                            // console.log(response.data);
                            if(response.data.ok === true) {
                                rs_datas_suc.push(response.data.result)
                            }else{
                                rs_error.push(response.data.result);
                                rs_datas_err.push(objService);
                            }

                            // reply.send(objService);
                        } else {
                            // reply.send({ ok: false });
                        }
                    // } catch (error: any) {
                    //     console.log(error);
                    //     reply.send({ ok: false, error: error.message });
                    // }
                } else {
                    // reply.send({ ok: false, error: 'Incorrect data!' });
                }
                await new Promise(resolve => setTimeout(resolve, delayInterval2));

            }
            // reply.status(StatusCodes.OK).send({result: rs_datas_suc});

        // } catch (error: any) {
        //     console.log(error.error);
        //     rs_err++;
        //     rs_error.push(error.error);
        //     rs_datas_err.push(objService);
        //     // reply.send({ ok: false, message: error.error.message , error.error: error, result: rs_datas_err });
        // }
    reply.send({ ok: true , result:{  success: rs_datas_suc ,error: rs_datas_err,message:rs_error ,cout:{total_success:rs_datas_suc.length,total_error:rs_datas_err.length}}});


    })

    cron.schedule(`${minute} ${hour} * * *`, async (req: FastifyRequest, reply: FastifyReply) => {
        // cron.schedule(`59 09 * * *`, async (req: FastifyRequest, reply: FastifyReply) => {
        let Today = Date.now();
        let x = Today - (1 * 24 * 60 * 60 * 1000);
        let yesterday = moment(x).tz('Asia/Bangkok').format('YYYY-MM-DD')
        console.log(`running a task every minute: ${minute} / hour: ${hour}`);
        console.log('YesterDay :', moment(yesterday).tz('Asia/Bangkok').format('YYYY-MM-DD'));


        let startDate: any = yesterday;
        let endDtae: any = yesterday;
        let rs_datas: any = [];
        let rs_visit = await hisModel.getVisit(db, startDate, endDtae);
        for (let x of rs_visit) {
            const hisSecretKey: any = process.env.API_HIS_SECRET_KEY || '';
            var token: any = jwt.sign({
                exp: Math.floor(Date.now() / 1000) + (2 * 60), // 2 minute
            }, hisSecretKey);

            let objService: any = {};
            let providerCode: any;
            let providerName: any;
            let profile: any = [];
            if (x.hn && x.seq) {
                // try {
                let rs_hospital = await hisModel.getHospital(db, x.hn);
                // console.log("rs_hospital:", rs_hospital);

                if (rs_hospital[0]) {
                    providerCode = rs_hospital[0].provider_code;
                    providerName = rs_hospital[0].provider_name;
                }
                let rs_profile = await hisModel.getProfile(db, x.hn, x.seq);
                // console.log("rs_profile:", rs_profile);
                if (rs_profile[0]) {
                    for (const rpro of rs_profile) {
                        let objPro = {
                            "hospcode": providerCode,
                            "hospname": providerName,
                            "hn": rpro.hn,
                            "cid": rpro.cid,
                            "title_name": rpro.title_name,
                            "first_name": rpro.first_name,
                            "last_name": rpro.last_name,
                            "moopart": rpro.moopart,
                            "addrpart": rpro.addrpart,
                            "tmbpart": rpro.tmbpart,
                            "amppart": rpro.amppart,
                            "chwpart": rpro.chwpart,
                            "brthdate": moment(rpro.brthdate).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                            "age": rpro.age,
                            "sex": rpro.sex,
                            "occupation": rpro.occupation,
                            "pttype_id": rpro.pttype_id,
                            "pttype_name": rpro.pttype_name,
                            "pttype_no": rpro.pttype_no,
                            "hospmain": rpro.hospmain,
                            "hospmain_name": rpro.hospmain_name,
                            "hospsub": rpro.hospsub,
                            "hospsub_name": rpro.hospsub_name,
                            "father_name": rpro.father_name,
                            "mother_name": rpro.mother_name,
                            "couple_name": rpro.couple_name,
                            "contact_name": rpro.contact_name,
                            "contact_relation": rpro.contact_relation,
                            "contact_mobile": rpro.contact_mobile,
                            "registdate": moment(rpro.registdate).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                            "visitdate": moment(rpro.visitdate).tz('Asia/Bangkok').format('YYYY-MM-DD')
                        }
                        profile.push(objPro);
                    }
                    objService.profile = profile;
                }

                let rs_allergy = await hisModel.getAllergyDetail(db, x.hn);
                // console.log("rs_allergy:", rs_allergy[0]);
                if (rs_allergy[0]) {
                    let allergy: any = [];
                    for (const ra of rs_allergy) {
                        let objAllergy = {
                            "hospcode": providerCode,
                            "hospname": providerName,
                            "drug_name": ra.drug_name,
                            "symptom": ra.symptom,
                            "begin_date": moment(ra.begin_date).tz('Asia/Bangkok').format('YYYY-MM-DD'),    //วันที่แพ้ยา
                            "daterecord": ra.daterecord     //วันที่บันทึกประวัติแพ้ยา
                        }
                        allergy.push(objAllergy);
                    }
                    objService.allergy = allergy;
                }

                let rs_services = await hisModel.getServices(db, x.hn, x.seq);
                // console.log("rs_services:", rs_services[0]);
                if (rs_services[0]) {
                    let pillness: any = [];
                    let physicals: any = [];
                    let nurtures: any = [];
                    let diagnosis: any = [];
                    let drugs: any = [];
                    let lab: any = [];
                    let procedure: any = [];
                    let xray: any = [];
                    let visit: any = [];
                    for (const v of rs_services) {
                        let objVisit = {
                            "hospcode": providerCode,
                            "hospname": providerName,
                            "seq": x.seq,
                            "hn": v.hn,
                            "an": v.an,
                            "title_name": v.title_name,
                            "first_name": v.first_name,
                            "last_name": v.last_name,
                            "date_serv": v.date_serv,
                            "time_serv": v.time_serv,
                            "department": v.department,
                        }
                        visit.push(objVisit);
                        objService.visit = visit;

                        let rs_pillness = await hisModel.getPillness(db, x.hn, v.date_serv, x.seq);
                        // console.log("rs_pillness:", rs_pillness[0]);
                        if (rs_pillness[0]) {
                            for (const ri of rs_pillness) {
                                let objPillness = {
                                    "hospcode": providerCode,
                                    "hospname": providerName,
                                    "seq": ri.seq,
                                    "pi": ri.hpi,
                                }
                                pillness.push(objPillness);
                            }
                            objService.pillness = pillness;
                        } else {
                            objService.pillness = [];
                        }

                        let rs_physicals = await hisModel.getPhysical(db, x.hn, v.date_serv, x.seq);
                        // console.log("rs_nurtures:", rs_physicals[0]);
                        if (rs_physicals[0]) {
                            for (const rp of rs_physicals) {
                                let objPhysicals = {
                                    "hospcode": providerCode,
                                    "hospname": providerName,
                                    "seq": rp.seq,
                                    "pe": rp.pe,
                                }
                                physicals.push(objPhysicals);
                            }
                            objService.physicals = physicals;
                        } else {
                            objService.physicals = [];
                        }

                        let rs_nurtures = await hisModel.getNurture(db, x.hn, v.date_serv, x.seq);
                        // console.log("rs_nurtures:", rs_nurtures[0]);
                        if (rs_nurtures[0]) {
                            for (const rn of rs_nurtures) {
                                let objNurtures = {
                                    "hospcode": providerCode,
                                    "hospname": providerName,
                                    "seq": rn.seq,
                                    "date_serv": moment(rn.date_serv).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                    "time_serv": rn.time_serv,
                                    "bloodgrp": rn.bloodgrp,
                                    "weight": rn.weight || 0,
                                    "height": rn.height || 0,
                                    "bmi": rn.bmi || 0,
                                    "temperature": rn.temperature,
                                    "pr": rn.pr,
                                    "rr": rn.rr,
                                    "sbp": rn.sbp,
                                    "dbp": rn.dbp,
                                    "symptom": rn.symptom,
                                    "pmh": rn.pmh,
                                    "depcode": rn.depcode,
                                    "department": rn.department,
                                    "movement_score": rn.movement_score,
                                    "vocal_score": rn.vocal_score,
                                    "eye_score": rn.eye_score,
                                    "oxygen_sat": rn.oxygen_sat,
                                    "gak_coma_sco": rn.gak_coma_sco,
                                    "pupil_right": rn.pupil_right,
                                    "pupil_left": rn.pupil_left,
                                    "diag_text": rn.diag_text || ''
                                }
                                nurtures.push(objNurtures);
                            }
                            objService.nurtures = nurtures;
                        } else {
                            objService.nurtures = [];
                        }

                        let rs_diagnosis = await hisModel.getDiagnosis(db, x.hn, v.date_serv, x.seq);
                        // console.log("rs_diagnosis:", rs_diagnosis[0]);
                        if (rs_diagnosis[0]) {
                            for (const rg of rs_diagnosis) {
                                let objDiagnosis = {
                                    "hospcode": providerCode,
                                    "hospname": providerName,
                                    "seq": rg.seq,
                                    "date_serv": moment(rg.date_serv).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                    "time_serv": rg.time_serv,
                                    "icd_code": rg.icd_code,
                                    "icd_name": rg.icd_name,
                                    "diag_type": rg.diag_type,
                                    "diag_note": rg.DiagNote,        //DiagNote
                                    "diagtype_id": rg.diagtype_id   //diagtype_id
                                }
                                diagnosis.push(objDiagnosis);
                            }
                            objService.diagnosis = diagnosis;
                        } else {
                            objService.diagnosis = [];
                        }

                        let rs_procedure = await hisModel.getProcedure(db, x.hn, v.date_serv, x.seq);
                        // console.log("rs_procedure:", rs_procedure[0]);
                        if (rs_procedure[0]) {
                            for (const rp of rs_procedure) {
                                let objProcedure = {
                                    "hospcode": providerCode,
                                    "hospname": providerName,
                                    "seq": rp.seq,
                                    "date_serv": moment(rp.date_serv).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                    "time_serv": rp.time_serv,
                                    "procedure_code": rp.procedure_code,
                                    "procedure_name": rp.procedure_name,
                                    "start_date": moment(rp.start_date).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                    "start_time": rp.start_time,
                                    "end_date": rp.end_date ? moment(rp.end_date).tz('Asia/Bangkok').format('YYYY-MM-DD') : rp.end_date,
                                    "end_time": rp.end_time
                                }
                                procedure.push(objProcedure);
                            }
                            objService.procedure = procedure;
                        } else {
                            objService.procedure = [];
                        }

                        let rs_drugs = await hisModel.getDrugs(db, x.hn, v.date_serv, x.seq);
                        // console.log("rs_drugs:", rs_drugs[0]);
                        if (rs_drugs[0]) {
                            for (const rd of rs_drugs) {
                                let objDrug = {
                                    "hospcode": providerCode,
                                    "hospname": providerName,
                                    "seq": rd.seq,
                                    "date_serv": moment(rd.date_serv).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                    "time_serv": rd.time_serv,
                                    "drug_name": rd.drug_name,
                                    "qty": rd.qty,
                                    "unit": rd.unit,
                                    "usage_line1": rd.usage_line1,
                                    "usage_line2": rd.usage_line2,
                                    "usage_line3": rd.usage_line3
                                }
                                drugs.push(objDrug);
                            }
                            objService.drugs = drugs;
                        } else {
                            objService.drugs = [];
                        }

                        let rs_lab = await hisModel.getLabs(db, x.hn, v.date_serv, x.seq);
                        // console.log("rs_lab:", rs_lab[0]);
                        if (rs_lab[0]) {
                            for (const rl of rs_lab) {
                                let standard_result: any;
                                if (rl.standard_result) {
                                    standard_result = rl.standard_result.toString();
                                } else {
                                    standard_result = rl.standard_result;
                                }
                                let date_serv: any = moment(rl.date_serv).tz('Asia/Bangkok').format('YYYY-MM-DD');
                                if (date_serv == 'Invalid date') { date_serv = v.date_serv }
                                let objLab = {
                                    "hospcode": providerCode,
                                    "hospname": providerName,
                                    "seq": x.seq,
                                    "date_serv": date_serv,
                                    "time_serv": rl.time_serv,
                                    "labgroup": rl.labgroup,
                                    "lab_name": rl.lab_name,
                                    "lab_result": rl.lab_result,
                                    "unit": rl.unit || null,
                                    "standard_result": standard_result || null,
                                }
                                lab.push(objLab);
                            }
                            objService.lab = lab;
                        } else {
                            objService.lab = [];
                        }

                        let rs_xray = await hisModel.getXray(db, x.hn, v.date_serv, x.seq);
                        // console.log("rs_xray:", rs_xray[0]);
                        if (rs_xray[0]) {
                            for (const v of rs_xray) {
                                let objXray = {
                                    "hospcode": providerCode,
                                    "hospname": providerName,
                                    "seq": x.seq,
                                    "xray_date": moment(v.xray_date).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                    "xray_name": v.xray_name,
                                }
                                xray.push(objXray);
                            }
                            objService.xray = xray;
                        } else {
                            objService.xray = [];
                        }
                    }
                }

                if (objService) {
                    const response: AxiosResponse = await serviceModels.getImportService(fastify.axios, token, objService);
                    rs_datas.push(objService);
                    // reply.send(objService);
                } else {
                    // reply.send({ ok: false });
                }

            }
            await new Promise(resolve => setTimeout(resolve, delayInterval));

        }

    });

    done();
}
