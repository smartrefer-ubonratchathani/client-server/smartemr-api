import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'

// model
import { HisUniversalHiModel } from '../../models/his_universal.model';
import { HisHiModel } from '../../models/his_hi.model';
import { HisHomcModel } from '../../models/his_homc.model';
import { HisHosxpv3Model } from '../../models/his_hosxpv3.model';
import { HisHosxpv3ArjaroModel } from '../../models/his_hosxpv3arjaro.model';
import { HisHosxpv4Model } from '../../models/his_hosxpv4.model';
import { HisHosxpv4PgModel } from '../../models/his_hosxpv4pg.model';
import { HisHosxpv4PCUModel } from '../../models/his_hosxpv4_pcu.model';
import { HisMbaseModel } from '../../models/his_mbase.model';
import { HisPhospModel } from '../../models/his_phosp.model';
import { HisHimproHiModel } from '../../models/his_himpro.model';
import { HisJhcisHiModel } from '../../models/his_jhcis.model';
import { HisUbaseModel } from '../../models/his_ubase.model';
import { HisSakonModel } from '../../models/his_sakon.model';
import { HisHomcUbonModel } from '../../models/his_homc_udon.model';
import { HisSoftConModel } from '../../models/his_softcon.model'
import { HisSoftConUdonModel } from '../../models/his_softcon_udon.model'
import { HisSsbModel } from '../../models/his_ssb.model'
import { HisPanaceaplusModel } from '../../models/his_panaceaplus.model'

export default async function login(fastify: FastifyInstance) {

  // ห้ามแก้ไข // 
  //-----------------BEGIN START-------------------//
  const provider = process.env.API_HIS_PROVIDER;
  const db = fastify.db;
  let hisModel: any;

  switch (provider) {
    case 'ubase':
      hisModel = new HisUbaseModel();
      break;
    case 'himpro':
      hisModel = new HisHimproHiModel();
      break;
    case 'jhcis':
      hisModel = new HisJhcisHiModel();
      break;
    case 'hosxpv3':
      hisModel = new HisHosxpv3Model();
      break;
    case 'hosxpv3arjaro':
      hisModel = new HisHosxpv3ArjaroModel();
      break;
    case 'hosxpv4':
      hisModel = new HisHosxpv4Model();
      break;
    case 'hosxpv4pg':
      hisModel = new HisHosxpv4PgModel();
      break;
    case 'hi':
      hisModel = new HisHiModel();
      break;
    case 'homc':
      hisModel = new HisHomcModel();
      break;
    case 'mbase':
      hisModel = new HisMbaseModel();
      break;
    case 'phosp':
      hisModel = new HisPhospModel();
      break;
    case 'universal':
      hisModel = new HisUniversalHiModel();
      break;
    case 'sakon':
      hisModel = new HisSakonModel();
      break;
    case 'homcudon':
      hisModel = new HisHomcUbonModel();
      break;
    case 'softcon':
      hisModel = new HisSoftConModel();
      break;
    case 'softconudon':
      hisModel = new HisSoftConUdonModel();
      break;
    case 'ssb':
      hisModel = new HisSsbModel();
      break;
    case 'panaceaplus':
      hisModel = new HisPanaceaplusModel();
      break;
    case 'hosxppcuv4':
      hisModel = new HisHosxpv4PCUModel();
      break;

    default:
    // hisModel = new HisModel();
  }
  //------------------BEGIN END------------------//
  fastify.post('/', async (request: FastifyRequest, reply: FastifyReply) => {
    const body: any = request.body
    const username = body.username
    const password = body.password
    console.log(body);

    try {
      console.log(username, '/', password);
      let res_login = await hisModel.getLogin(db, username, password);
      console.log(res_login);
      const date = new Date();
      const iat = Math.floor(date.getTime() / 1000);
      const exp = Math.floor((date.setDate(date.getDate() + 7)) / 1000);

      if (res_login[0]) {   
        if(res_login[0].username && res_login[0].hcode){
          const user: any = res_login[0]
          const info: any = {
            id: user.username,
            name: user.fullname,
            hcode: user.hcode,
            email: 'thawatchai.sea2@gmail.com',
            avatar: 'assets/images/avatars/brian-hughes.jpg',
            status: 'online'
  
          }
          const payload: any = {
            iat: iat,
            iss: 'Fuse',
            exp: exp
          }
          const token = fastify.jwt.sign(payload)
          
          reply.send({ accessToken: token, user: info, tokenType: 'bearer' })
        }else {
          reply.send({ ok: false, error: `status: ไมพบ User ผู้ใช้งาน` });
        }   

      } else {
        reply.send({ ok: false, error: `status: error` });
      }
    } catch (error) {
      reply.send({ ok: false, error: error });
    }
  });

}
