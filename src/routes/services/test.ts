import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
export default async (fastify: FastifyInstance, _options: any, done: any) => {

    fastify.get('/', {
        preHandler: [
        //   fastify.guard.role(['admin', 'nurse', 'doctor'])
          // fastify.guard.scope(['nurse.ward','nurse.create'])
        ],
      },
        async (request: FastifyRequest, reply: FastifyReply) => {
            reply.status(200).send({ok:true , message:'Smart EMR'});
        })
    
    done();   
}