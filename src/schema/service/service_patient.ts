import S from 'fluent-json-schema'

const querySchema = S.object()
  .prop('an', S.string().maxLength(15).required())
  .prop('dateStart', S.string().maxLength(10).required())
  .prop('dateEnd', S.string().maxLength(10).required())
  .prop('limit', S.number().maximum(100))
  .prop('offset', S.number().minimum(0))

export default {
    querystring: querySchema
}