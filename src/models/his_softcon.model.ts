import { Knex } from 'knex';
export class HisSoftConModel {
    async getLogin(db: Knex, username: any, password: any) {
        // console.log(db);
        
        let data = await db.raw(`
        select ltrim(rtrim(name)) as username
        ,ltrim(rtrim(displayname)) as fullname
        ,'14201' as hcode
        from SoftCon.[User] u 
        where ltrim(rtrim(u.name)) = '${username}'
        and u.password = dbo.[ComputeHash]('${password}')
        `);
        // console.log(data);
        
        return data;
    }
    async getVisit(db: Knex, startDate: any, endDtae: any) {
        let data = await db.raw(`
        select top 1 r.VisitKey as seq
        ,t.name as title_name
        ,p.firstname as first_name
        ,p.lastname as last_name
        ,convert(varchar(10),r.DocDT,121) as date_serv
        ,convert(varchar,r.DocDT,108) as time_serv
        ,s.Name as department
        ,'OPD' as dep
        from referout r
        join person p on r.patientkey = p.personkey
        join Title t on p.titlekey = t.titlekey
        join ServiceUnit s on r.serviceunitkey = s.serviceunitkey
        where r.ClinicVisitKey is not null
        and convert(varchar(10),r.DocDT,121) between'${startDate}' and '${endDtae}'
        union all
        select top 1 r.VisitKey as seq
        ,t.name as title_name
        ,p.firstname as first_name
        ,p.lastname as last_name
        ,convert(varchar(10),r.DocDT,121) as date_serv
        ,convert(varchar,r.DocDT,108) as time_serv
        ,s.Name as department
        ,'IPD' as dep
        from referout r
        join person p on r.patientkey = p.personkey
        join Title t on p.titlekey = t.titlekey
        join ServiceUnit s on r.serviceunitkey = s.serviceunitkey
        where r.AdmitKey is not null
        and convert(varchar(10),r.DocDT,121) between'${startDate}' and '${endDtae}'
        `);
        return data;
    }
    async getServices(db: Knex, hn: any, seq: any) {
        let data = await db.raw(`
        select top 1 r.VisitKey as seq
        ,t.name as title_name
        ,p.firstname as first_name
        ,p.lastname as last_name
        ,convert(varchar(10),r.DocDT,121) as date_serv
        ,convert(varchar,r.DocDT,108) as time_serv
        ,s.Name as department
        ,'OPD' as dep
        from referout r
        join person p on r.patientkey = p.personkey
        join Title t on p.titlekey = t.titlekey
        join ServiceUnit s on r.serviceunitkey = s.serviceunitkey
        where r.ClinicVisitKey is not null
        and r.VisitKey = '${seq}'
        union all
        select top 1 r.VisitKey as seq
        ,t.name as title_name
        ,p.firstname as first_name
        ,p.lastname as last_name
        ,convert(varchar(10),r.DocDT,121) as date_serv
        ,convert(varchar,r.DocDT,108) as time_serv
        ,s.Name as department
        ,'IPD' as dep
        from referout r
        join person p on r.patientkey = p.personkey
        join Title t on p.titlekey = t.titlekey
        join ServiceUnit s on r.serviceunitkey = s.serviceunitkey
        where r.AdmitKey is not null
        and r.VisitKey = '${seq}'
        `);
        return data;
    }
    async getProfile(db: Knex, hn: any, seq: any) {
        let data = await db.raw(`
        select top 1
        pt.Code as hn
        ,r.patientkey 
        ,(case when p.cid = '' or p.cid is null then '9999999999999' else p.cid end) as cid
        ,t.name as title_name
        ,p.firstname as first_name
        ,p.lastname as last_name
        ,case when p.moo is null  then '00' when LEN(p.moo)= 1  then '0'+ltrim(rtrim(p.moo))
            else ltrim(rtrim(p.moo))
        end as moopart
        ,p.addressno as addrpart
        ,right(p.thsubdistrictkey,2)  as tmbpart
        ,right(left(p.THSubdistrictKey,4),2) as amppart
        ,case when p.thprovincekey = '' or p.thprovincekey is null then '34' else p.thprovincekey end as chwpart
        ,convert(varchar(10),p.birthdt,121) as brthdate
        ,RIGHT('000'+LTRIM(STR(year(getdate()) - year(p.birthdt))),2) + ''+ 
         RIGHT('00'+LTRIM(STR(ABS(DATEDIFF(month, p.birthdt, getdate()))%12)),0)+''+ 
         RIGHT('00'+LTRIM(STR(replace(DATEDIFF(DAY,DAY(p.birthdt),DAY(GETDATE())),'-',''))),0)  as age 
        ,replace(p.genderkey,'-','') as sex
        ,(case when p.genderkey = '-1' then 'ชาย' 
         when p.genderkey = '-2' then 'หญิง'
          else 'ไม่ระบุเพศ' end) as sexname
        ,o.Name as occupation
        ,mc.Code as pttype_id  --รหัสสิทธิ์การรักษา
        ,mc.Name as pttype_name --สิทธิ์การรักษา
        ,r.MCDocID as pttype_no --เลขที่สิทธิ์การรักษา
        ,h.Code as hospmain --รหัส รพ.สิทธิ์การรักษาหลัก
        ,l.Name as hospmain_name --รพ.สิทธิ์การรักษา
        ,hr.Code as hospsub -- รหัส รพ.สิทธิ์การรักษารอง
        ,lr.Name as hospsub_name --รพ.สิทธิ์การรักษารอง
        ,convert(varchar(10),pt.docdt,121) as registdate 
        ,convert(varchar(10),r.docdt,121) as visitdate
        ,concat(p.fatherfirstname,' ',p.fatherlastname) as father_name
        ,concat(p.motherfirstname,' ',p.motherlastname) as mother_name
        ,concat(p.spousefirstname,' ',p.spouselastname) as couple_name
        ,p.contactpersonname as contact_name
        ,p.contactpersonrelation as contact_relation
        ,p.contactpersontelephone as contact_mobile
        from person p
        join referout r on p.personkey = r.patientkey 
        left join Title t on p.titlekey = t.titlekey
        left join Occupation o on p.occupationkey = o.occupationkey
        left join patient pp on p.personkey = pp.patientkey
		LEFT JOIN dbo.Patient pt ON pt.PatientKey = r.PatientKey
		LEFT JOIN dbo.Admit a ON a.AdmitKey = r.AdmitKey
		LEFT JOIN dbo.ClinicVisit cv ON cv.ClinicVisitKey = r.ClinicVisitKey
		JOIN dbo.MC ON mc.MCKey = r.MCKey
		LEFT JOIN dbo.Hospital h ON h.HospitalKey = r.HospitalKey
		LEFT JOIN dbo.Hospital hr ON hr.HospitalKey = r.MinorHospitalKey
		LEFT JOIN dbo.LegalEntity l ON l.LegalEntityKey = h.HospitalKey
		LEFT JOIN dbo.LegalEntity lr ON lr.LegalEntityKey = r.MinorHospitalKey
        where pt.Code = '${hn}' and r.VisitKey = '${seq}'
        `);
        return data[0];
    }
    async getHospital(db: Knex, hn: any) {
        let data = await db.raw(`
        SELECT '14201' as provider_code,'โรงพยาบาลมะเร็งอุบลราชธานี' as provider_name
        `);
        return data;
    }
    async getAllergyDetail(db: Knex, hn: any) {
        let data = await db.raw(`
        SELECT drg.name as drug_name
        ,adr.note as symptom 
        ,convert(varchar(10),adr.adrcardissuedate,121) as begin_date
        ,convert(varchar(10),adr.recorddt,121) as daterecord
        FROM PatientADR adr
        inner join DrugGenericName drg on adr.druggenericnamekey = drg.druggenericnamekey
		JOIN dbo.Patient p ON p.PatientKey = adr.PatientKey
        where p.Code = '${hn}'
        `);
        return data;
    }
    async getDiagnosis(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        SELECT r.VisitKey as seq, 
        convert(varchar(10),r.DocDT,121) as date_serv,
        convert(varchar,r.DocDT,108) as time_serv ,
        d.Icd10Code as icd_code,
        d.Icd10Name as icd_name,
        d.DXTypeKey as diag_type, 
        REPLACE(REPLACE(r.DXNote, CHAR(13), ''), CHAR(10), '') AS DiagNote,
        d.DXTypeKey as diagtype_id
        FROM ReferOut r 
        LEFT JOIN ReferOutDX d on d.ReferOutKey = r.ReferOutKey
        WHERE r.VisitKey = ${seq}
        `);
        return data;
    }
    async getDrugs(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        select  
        mh.VisitKey as seq
        ,convert(varchar(10),mh.docdt,121) as date_serv
        ,convert(varchar,mh.docdt,108) as time_serv
        ,mi.itemname as drug_name
        ,mi.IssueQty as qty
        ,mi.itemunitname as unit 
        ,REPLACE(REPLACE(mi.usagenote, CHAR(13), ''), CHAR(10), '')  as usage_line1
        ,'' as usage_line2
        ,'' as usage_line3
        from medrequest mi
        inner join medrequestheader mh on mi.medrequestheaderkey = mh.medrequestheaderkey
        where mh.VisitKey ='${seq}'
        order by docdt DESC
        `);
        return data;
    }
    async getLabs(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        SELECT
        l.VisitKey as seq,
		convert(varchar(10),l.DocDT,121) as date_serv,
		convert(varchar,l.DocDT,108) as time_serv ,
		lx.THTHText as labgroup,
		lr.ItemName as lab_name,        
		ISNULL( lod.Name , ls.ResultValue) +
		CASE WHEN lod.Name IS NOT NULL AND ls.ResultValue IS NOT NULL THEN ls.ResultValue ELSE '' END as lab_result,        
		lr.ItemUnitName as unit, 
		CASE when (CASE WHEN (lo.HasGenderVar = 1) THEN ISNULL(CAST(CAST(lor.MinRefValue AS decimal(19,3)) AS nvarchar(10)) + ' - ' + CAST(CAST(lor.MaxRefValue AS decimal(19,3)) AS nvarchar(10)),lo.NormalValueNote)  ELSE ISNULL(CAST(CAST(ls.MinRefValue AS decimal(19,3)) AS nvarchar(10)) + ' - ' + CAST(CAST(ls.MaxRefValue AS decimal(19,3)) AS nvarchar(10)), lo.NormalValueNote) END ) is null then '.' else 
		CASE WHEN (lo.HasGenderVar = 1) THEN ISNULL(CAST(CAST(lor.MinRefValue AS decimal(19,3)) AS nvarchar(10)) + ' - ' + CAST(CAST(lor.MaxRefValue AS decimal(19,3)) AS nvarchar(10)),lo.NormalValueNote)  ELSE ISNULL(CAST(CAST(ls.MinRefValue AS decimal(19,3)) AS nvarchar(10)) + ' - ' + CAST(CAST(ls.MaxRefValue AS decimal(19,3)) AS nvarchar(10)), lo.NormalValueNote) END
		end as standard_result
        from LabRequestHeader l
		JOIN dbo.Person ps ON ps.PersonKey = l.PatientKey
		JOIN LabRequest lr on l.LabRequestHeaderKey = lr.LabRequestHeaderKey
		LEFT JOIN LabResult ls on ls.LabRequestKey = lr.LabRequestKey
		LEFT JOIN LabOutput lo on lo.LabOutputKey = ls.LabOutputKey
		LEFT JOIN dbo.LabOutputDomain lod ON lod.LabOutputDomainKey = ls.LabOutputDomainKey
		JOIN SoftCon.ReferenceX lx ON lx.ReferenceKey = l.LabTypeKey AND EnumClassName = 'LabType'
		LEFT JOIN dbo.LabOutputRef lor ON lor.LabOutputKey = lo.LabOutputKey AND lor.GenderKey = ps.GenderKey        
		WHERE l.VisitKey = '${seq}' and isnull(ls.ResultValue,'') <> ''
		AND l.IsCanceled = 0
        `);
        return data;
    }
    async getProcedure(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
     SELECT 
        v.PatientKey as pid,
        v.VisitKey as seq,
        convert(varchar(10),v.DocDT,121) as date_serv, 
        convert(varchar,v.DocDT,108) as time_serv, 
        i.Code as procedure_code,	
        r.ItemName as procedure_name,
        convert(varchar(10),h.DocDT,121) as start_date,	
        convert(varchar,h.DocDT,108) as start_time,
        convert(varchar(10),h.ExpectIssueDate,121) as end_date,
        convert(varchar,h.ExpectIssueTime,108) as end_time
		from ORRequestHeader h 
		INNER JOIN Visit v on  v.VisitKey = h.VisitKey
		INNER JOIN ORRequest r on r.ORRequestHeaderKey = h.ORRequestHeaderKey
		JOIN dbo.Item i ON i.ItemKey = r.ItemKey
		WHERE h.VisitKey = '${seq}'
        `);
        return data;
    }
    async getNurture(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        select top 1 r.VisitKey as seq,
        convert(varchar(10),v.DocDT,121) as date_serv, 
        convert(varchar,v.DocDT,108) as time_serv, 
        '' as bloodgrp,
        c.Weight as weight,
        c.Height as height,
        '' as bmi,
        vs.Temp as temperature,
        vs.PR as pr,
        vs.RR as rr,
        vs.SystolicBP as sbp,
        vs.DiastolicBP as dbp,
        c.CCNote as symptom,
        s.Code as depcode,
        s.Name as department,
        '' as movement_score,
        '' as vocal_score,
        '' as eye_score,
        vs.O2Sat as oxygen_sat,
        vs.ConciousScore as gak_coma_sco,
        '' as diag_text,
        '' as pupil_right,
        '' as pupil_left
        FROM ReferOut r
        LEFT JOIN Visit v on r.VisitKey = v.VisitKey
        INNER JOIN ClinicVisit c ON c.ClinicVisitKey = r.ClinicVisitKey
        LEFT JOIN ClinicVisitVS vs on vs.ClinicVisitKey = r.ClinicVisitKey
        LEFT JOIN ServiceUnit s on s.serviceunitkey = c.serviceunitkey
        WHERE r.ClinicVisitKey is not null
        and v.VisitKey = '${seq}'

        UNION all 

        select 	top 1 r.VisitKey as seq,
        convert(varchar(10),a.DocDT,121) as date_serv, 
        convert(varchar,a.DocDT,108) as time_serv, 
        '' as bloodgrp,
        a.Weight as weight,
        a.Height as height,
        '' as bmi,
        vs.Temp as temperature,
        vs.PR as pr,
        vs.RR as rr,
        vs.SystolicBP as sbp,
        vs.DiastolicBP as dbp,
        a.PENurseNote as symptom,
        s.Code as depcode,
        s.Name as department,
        '' as movement_score,
        '' as vocal_score,
        '' as eye_score,
        vs.O2Sat as oxygen_sat,
        vs.ConciousScore as gak_coma_sco,
        '' as diag_text,
        '' as pupil_right,
        '' as pupil_left
        FROM ReferOut r
        INNER JOIN Admit a ON a.AdmitKey = r.AdmitKey
        INNER JOIN AdmitVS vs on vs.AdmitKey = a.AdmitKey
        LEFT JOIN ClinicVisit c ON c.ClinicVisitKey = a.ClinicVisitKey
        LEFT JOIN ServiceUnit s on c.serviceunitkey = s.serviceunitkey
        WHERE r.AdmitKey is not null 
        and a.VisitKey = '${seq}'
        `);
        return data;
    }
    async getPhysical(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        SELECT c.VisitKey as seq , REPLACE(REPLACE(c.PENote, CHAR(13), ''), CHAR(10), '') as pe FROM ClinicVisit c WHERE  c.VisitKey = ${seq}
        `);
        return data;
    }
    async getPillness(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        SELECT c.VisitKey as seq , REPLACE(REPLACE(c.PINote, CHAR(13), ''), CHAR(10), '') as hpi FROM ClinicVisit c 
WHERE c.VisitKey = ${seq}
        `);
        return data;
    }
    async getXray(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        SELECT h.DocDT, x.ItemName  
        FROM RadRequest as x  
        INNER JOIN RadRequestHeader h on x.RadRequestHeaderKey = h.RadRequestHeaderKey
        WHERE h.VisitKey = '${seq}'
        `);
        return data;
    }
}

