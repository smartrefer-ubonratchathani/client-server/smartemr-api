import { AxiosInstance } from "axios";

export class ServiceModels {
  getImportService(axios: AxiosInstance, token: any, datas: any) {
    const url: any = '/importService';
    return axios.post(url,datas ,{
      headers: {
        'Authorization': 'Bearer ' + token,
      }
    });
  }
  getHospitalByCid(axios: AxiosInstance, token: any, cid: any) {
    console.log(token);
    
    const url: any = `/report/query/hospital/${cid}`;
    return axios.get(url ,{
      headers: {
        'Authorization': 'Bearer ' + token,
      }
    });
  }
  getService(axios: AxiosInstance, token: any, hospcode: any, cid: any) {
    const url: any = `/report/query/service/${hospcode}/${cid}`;
    return axios.get(url ,{
      headers: {
        'Authorization': 'Bearer ' + token,
      }
    });
  }
  getServiceProfile(axios: AxiosInstance, token: any, hospcode: any, hn: any, seq: any) {
    const url: any = `/report/query/service-profile/${hospcode}/${hn}/${seq}`;
    return axios.get(url ,{
      headers: {
        'Authorization': 'Bearer ' + token,
      }
    });
  }
  saveLogs(axios: AxiosInstance, token: any, datas: any) {
    const url: any = `/query/saveLogs`;
    return axios.post(url,datas ,{
      headers: {
        'Authorization': 'Bearer ' + token,
      }
    });
  }
}