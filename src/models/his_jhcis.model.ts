import { Knex } from 'knex';
var md5 = require('md5');
export class HisJhcisHiModel {
  async getLogin(db: Knex, username: any, password: any) {
    let pass = md5(password).toUpperCase();
    let data = await db.raw(`
        SELECT  user.username , CONCAT(user.fname, ' ', user.lname) as fullname , user.pcucode as hcode  FROM user
		    WHERE user.username = '${username}' and ((user.password = '${pass}') or (user.password = '${password}')) 
        `);
    return data[0];
  }
  async getVisit(db: Knex, startDate: any, endDtae: any) {
    let data = await db.raw(`
        SELECT visit.visitno AS seq,visit.pid as hn,
        (SELECT ctitle.titlename FROM ctitle WHERE ctitle.titlecode = person.prename ) AS title_name,
        person.fname,person.lname,visit.visitdate,visit.timestart, 'OPD'  AS 'department'  
        FROM visit INNER JOIN person ON visit.pcucode = person.pcucodeperson AND visit.pid = person.pid
        WHERE visit.visitdate between'${startDate}' and '${endDtae}'
        `);
    return data[0];
  }
  async getServices(db: Knex, hn: any, seq: any) {
    let data = await db.raw(`
        SELECT visit.visitno AS seq,visit.pid as hn,
        (SELECT ctitle.titlename FROM ctitle WHERE ctitle.titlecode = person.prename ) AS title_name,
        person.fname,person.lname,visit.visitdate,visit.timestart, 'OPD'  AS 'department'  
        FROM visit INNER JOIN person ON visit.pcucode = person.pcucodeperson AND visit.pid = person.pid
        WHERE visit.pid = '${hn}' AND  visit.visitno = '${seq}'`);
    return data[0];
  }
  async getProfile(db: Knex, hn: any, seq: any) {
    let data = await db.raw(`SELECT person.pid, person.idcard AS cid , (SELECT ctitle.titlename FROM ctitle WHERE ctitle.titlecode = person.prename ) AS title_name
       ,person.fname AS first_name, person.lname AS last_name,
        IFNULL(if(LENGTH(person.mumoi)=0,'00',if(LENGTH(person.mumoi) < 2 ,CONCAT('0',person.mumoi),person.mumoi ) ), '00') AS moopart,
        IFNULL(if(LENGTH(person.hnomoi)=1,'00',person.hnomoi), '00')  AS addrpart,
        IFNULL(if(LENGTH(person.subdistcodemoi)=0,'00',person.subdistcodemoi), '00')  AS tmbpart
        , IFNULL(if(LENGTH(person.distcodemoi)=0,'00',person.distcodemoi), '00') AS amppart,
         IFNULL(if(LENGTH(person.provcodemoi)=0,'00',person.provcodemoi), '00')  AS chwpart, 

    person.birth AS brthdate
        ,cast(concat(lpad(timestampdiff(year,person.birth,now()),3,'0'),'-'
        ,lpad(mod(timestampdiff(month,person.birth,now()),12),2,'0'),'-'
        ,lpad(if(DAYOFMONTH(person.birth)>DAYOFMONTH(now())
        ,day(LAST_DAY(SUBDATE(now(),INTERVAL 1 month)))-DAYOFMONTH(person.birth)+DAYOFMONTH(now())
        ,DAYOFMONTH(now())-DAYOFMONTH(person.birth)),2,'0')) as char(9)) as age
        ,if(person.sex = 1,'ชาย','หญิง') as sex
        ,(select occupaname from coccupa where occupacode=person.occupa limit 1) as occupation
        ,person.rightcode AS pttype_id, (SELECT cright.rightname FROM cright WHERE cright.rightcode = person.rightcode ) AS pttype_name
        ,  person.rightno AS pttype_no, person.hosmain AS hospmain
        , (SELECT chospital.hosname FROM chospital WHERE chospital.hoscode = person.hosmain) AS  hospmain_name
        , person.hossub AS hospsub,(SELECT chospital.hosname FROM chospital WHERE chospital.hoscode = person.hossub) AS hospsub_name
				, person.dateupdate AS  registdate, ifnull(person.datein,'') AS visitdate
        , ifnull(person.father,'') AS father_name, ifnull(person.mother,'') AS mother_name, ifnull(person.mate,'') AS couple_name
				, '' AS contact_name, '' AS contact_relation, ifnull(person.mobile,'') AS contact_mobile
        FROM person 
        WHERE pid ='${hn}'`);
    return data[0];
  }
  async getHospital(db: Knex, hn: any) {
    let data = await db.raw(`
        SELECT  office.offid AS provider_code, chospital.hosname AS provider_name 
         FROM office 
         INNER JOIN chospital ON office.offid = chospital.hoscode
         where offid = (
           select distinct pcucode from user where officertype=3 limit 1
         )
         `);
    return data[0];
  }
  async getAllergyDetail(db: Knex, hn: any) {
    let data = await db.raw(`
        SELECT cdrug.drugname AS ndrug_name, IFNULL(personalergic.symptom,0) AS symptom , personalergic.daterecord as begin_date, personalergic.daterecord AS daterecord
        FROM personalergic INNER JOIN cdrug ON personalergic.drugcode = cdrug.drugcode24
       WHERE personalergic.pid ='${hn}'`);
    return data[0];
  }
  async getDiagnosis(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
        
        SELECT visit.visitno AS seq, visit.visitdate AS  date_serv, IFNULL(visit.timestart,'00:00:00') AS time_serv
        ,replace(visitdiag.diagcode,'.','') AS icd_code, cdisease.diseasename as icd_name
        ,IF(visitdiag.dxtype = '01','1','0') AS diag_type,visit.symptomsco AS DiagNote,right(visitdiag.dxtype,1) AS diagtype_id
        FROM visit INNER JOIN visitdiag  ON visit.visitno = visitdiag.visitno 
		    INNER JOIN cdisease ON visitdiag.diagcode = cdisease.diseasecode
    WHERE visit.visitno ='${seq}'`);
    return data[0];
  }
  async getDrugs(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
        select visitdrug.visitno AS seq, visit.visitdate AS date_serv, visit.timestart AS time_serv, cdrug.drugname AS drug_name,
        visitdrug.unit AS qty,
 (SELECT cdrugunitsell.unitsellname FROM cdrugunitsell WHERE cdrugunitsell.unitsellcode = cdrug.unitsell) AS unit,
 visitdrug.dose AS usage_line1, '' AS usage_line2, '' AS usage_line3
        FROM visitdrug INNER JOIN visit ON visitdrug.visitno = visit.visitno
        INNER JOIN  cdrug  ON visitdrug.drugcode = cdrug.drugcode24
        WHERE visitdrug.visitno = '${seq}'`);
    return data[0];
  }
  async getLabs(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
    SELECT visit.visitdate AS date_serv, visit.timestart AS time_serv,'sugarblood' AS labgroup
    ,(CASE WHEN (visitlabsugarblood.typetesting IS NULL OR visitlabsugarblood.typetesting='1') AND visitlabsugarblood.foodsuspend='0' THEN '0531101'
          WHEN (visitlabsugarblood.typetesting IS NULL OR visitlabsugarblood.typetesting='1') AND visitlabsugarblood.foodsuspend='1' THEN '0531102'
          WHEN (visitlabsugarblood.typetesting='2') AND visitlabsugarblood.foodsuspend='0' THEN '0531002' 
          WHEN (visitlabsugarblood.typetesting='2') AND visitlabsugarblood.foodsuspend='1' THEN '0531004' 
          ELSE '0531101' END) AS  lab_name
    ,visitlabsugarblood.sugarnumdigit AS lab_result
    ,' mg%' AS unit
    ,'60-100' AS standard_result
    FROM visitlabsugarblood 
    INNER JOIN visit ON visitlabsugarblood.visitno = visit.visitno 
    where visit.visitno ='${seq}'
    UNION
    SELECT visit.visitdate AS date_serv, visit.timestart AS time_serv, visitlabchcyhembmsse.labcode AS labgroup
    ,ifnull((SELECT clabchcyhembmsse.labname FROM clabchcyhembmsse WHERE clabchcyhembmsse.labcode = visitlabchcyhembmsse.labcode limit 1),'') AS lab_name
    ,visitlabchcyhembmsse.labresultdigit AS lab_result
    ,ifnull((SELECT clabchcyhembmsse.unit FROM clabchcyhembmsse WHERE clabchcyhembmsse.labcode = visitlabchcyhembmsse.labcode limit 1),'') AS unit
    ,ifnull((SELECT clabchcyhembmsse.normalrangel FROM clabchcyhembmsse WHERE clabchcyhembmsse.labcode = visitlabchcyhembmsse.labcode limit 1),'') AS standard_result
    FROM visitlabchcyhembmsse INNER JOIN visit ON visitlabchcyhembmsse.visitno = visit.visitno where visit.visitno = '${seq}'
    `);
    return data[0];
  }
  async getProcedure(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
        SELECT  visit.pid AS pid, visit.visitno AS  seq, visit.visitdate AS date_serv, visit.timestart AS	time_serv
        ,cdrug.drugcode procedure_code, cdrug.drugname AS procedure_name
        , visit.visitdate AS start_date, visit.timestart AS start_time, visit.visitdate AS end_date, visit.timeend AS end_time
        from visit INNER JOIN visitdrug ON visit.visitno = visitdrug.visitno
        INNER JOIN cdrug ON visitdrug.drugcode = cdrug.drugcode  AND cdrug.drugtype = '02'
        where  visit.visitno = '${seq}'
        `);
    return data[0];
  }
  async getNurture(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
        select  visit.visitno AS seq,visit.visitdate AS date_serv,visit.timestart AS time_serv, 
          (SELECT person.bloodgroup FROM person WHERE person.pcucodeperson = visit.pcucodeperson AND person.pid=visit.pid ) AS bloodgrp,
          visit.weight AS weight,visit.height AS height,
         visit.weight/POW( 1.5,2)   AS bmi,visit.temperature AS temperature,
         visit.pulse AS pr,visit.respri AS rr, SUBSTRING_INDEX(visit.pressure,'/',1) AS sbp,
         SUBSTRING_INDEX(visit.pressure,'/',-1) AS dbp,visit.symptoms AS symptom , '10100' AS depcode, 'OPD' AS department,
         '' AS movement_score,'' AS vocal_score,'' AS eye_score,'' AS oxygen_sat,'' AS gak_coma_sco,
         visit.diagnote AS diag_text,'' AS pupil_right,'' AS pupil_left
        FROM visit 
        WHERE visit.visitno = '${seq}'
       
        `);
    return data[0];
  }
  async getPhysical(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
        SELECT  visit.visitno AS seq , visit.vitalcheck AS pe FROM visit WHERE visit.visitno = '${seq}';
        `);
    return data[0];
  }
  async getPillness(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
        SELECT visit.visitno AS seq ,visit.symptomsco AS hpi FROM visit WHERE visit.visitno = '${seq}'
        `);
    return data[0];
  }
  async getXray(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
        SELECT '' AS xray_date ,'' AS xray_name 
        `);
    return null;
  }
}
