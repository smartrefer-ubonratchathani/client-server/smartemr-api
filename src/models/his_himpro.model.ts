import { Knex } from 'knex';
export class HisHimproHiModel {
  async getLogin(db: Knex, username: any, password: any) {
    let data = await db.raw(`
       SELECT LOWER(userlogin) as 'username',username as 'fullname',pcucode as 'hcode' 
       FROM hosdata.user 
       WHERE userlogin = '${username}' and RIGHT(cid,4)='${password}' and pcucode != '' and histpt = '1'`);
    return data[0];
  }
  async getVisit(db: Knex, startDate: any, endDtae: any) {
    let cmd = `SELECT o.docno as 'seq',
    o.hn 'hn',i.an as 'an',p.pttitle 'title_name',p.ptfname 'first_name',
    p.ptlname 'last_name',o.regdate 'date_serv',o.timereg 'time_serv',
    c.NAME as 'department'
    FROM opd.opd o
    INNER JOIN pt.pt p on o.hn = p.hn
    INNER JOIN hos.clinic c on o.clinic = c.code
    left join ipd.ipd i on o.regdate = i.regdate and o.frequency = i.frequency and o.hn = i.hn
    WHERE o.regdate between'${startDate}' and '${endDtae}'`;
    let data = await db.raw(cmd);
    console.log("SQL Command Services : " + cmd);
    return data[0];
    // LEFT JOIN ipd.ipd i on o.regdate = i.regdate and o.hn = i.hn and o.frequency = i.frequency
  }
  async getServices(db: Knex, hn: any, seq: any) {
    let cmd = `SELECT o.docno 'seq',
    o.hn 'hn',i.an as 'an',p.pttitle 'title_name',p.ptfname 'first_name',
    p.ptlname 'last_name',o.regdate 'date_serv',o.timereg 'time_serv',
    c.NAME as 'department'
    FROM opd.opd o
    INNER JOIN pt.pt p on o.hn = p.hn
    INNER JOIN hos.clinic c on o.clinic = c.code
    left join ipd.ipd i on o.regdate = i.regdate and o.frequency = i.frequency and o.hn = i.hn
    WHERE o.hn = '${hn}' and o.docno = '${seq}' `;
    // and o.hn = SeqSmartReferToSeqSendRefer('${seq}')`;
    let data = await db.raw(cmd);
    console.log("SQL Command Services : " + cmd);
    return data[0];
  }
  async getProfile(db: Knex, hn: any, seq: any) {
    let cmd = `SELECT p.hn,REPLACE(cardid,'-','') 'cid',pttitle 'title_name',
    ptfname 'first_name',ptlname 'last_name',
    if(ptsex='sx1','ชาย','หญิง')'sex',
    ptvillage 'moopart',ptaddress 'addrpart',
    pttambon 'tmbpart',ptamphur 'amppart',
    ptprovince 'chwpart',ptdob 'brthdate',
    concat(right(concat('000',timestampdiff(YEAR,ptdob,NOW())),3) ,'-',right(concat('00',timestampdiff(month,ptdob,NOW())-(timestampdiff(year,ptdob,NOW())*12)),2), '-',right(concat('00',timestampdiff(day,date_add(ptdob,interval (timestampdiff(month,ptdob,NOW())) month),curdate())),2)) 'age',
    c.ptclass 'pttype_id',
    (SELECT i.NAME FROM hos.insclasses i WHERE c.ptclass = i.code)'pttype_name',
    c.classNo 'pttype_no',c.mainhos 'hospmain',hos.getHospitalName(c.mainhos)'hospmain_name',
    c.subhos 'hospsub',hos.getHospitalName(c.subhos)'hospsub_name',p.updatedate 'registdate',
    p.lastvisitdate 'visitdate',CONCAT(p.fattitle,p.fatfname,' ',p.fatlname)'father_name',
    CONCAT(p.mottitle,p.motfname,' ',p.motlname)'mother_name',
    '' as 'couple_name',CONCAT(p.contacttitle,p.contactfname,' ',p.contactlname)'contact_name',
    (SELECT h.NAME FROM hos.codeinhos h WHERE h.code = p.relation)'contact_relation',
    p.ptphone 'contact_mobile' FROM pt.pt p LEFT JOIN pt.ptclass c on p.hn = c.hn
    WHERE p.hn = '${hn}' GROUP BY p.hn `;
    let data = await db.raw(cmd);
    console.log("getProfile : " + cmd);
    return data[0];
  }
  async getHospital(db: Knex, hn: any) {
    let data = await db.raw(`
        SELECT codehos 'provider_code',nameHos 'provider_name' FROM hosdata.confighos`);
    return data[0];
  }
  async getAllergyDetail(db: Knex, hn: any) {
    let data = await db.raw(`SELECT listname 'ndrug_name',
    listsign 'symptom',daterecord 'begin_date',daterecord
    FROM pt.ptallergy
    WHERE hn = '${hn}'
    and listname != 'ไม่มีประวัติแพ้'`); //Edit 2022-08-30
    return data[0];
  }
  async getDiagnosis(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`SELECT '${seq}' as 'seq',
    s.regdate 'date_serv',s.timereg as 'time_serv',
    d.diag 'icd_code',d.descrip 'icd_name',
    d.dxtype 'diag_type','' as 'DiagNote',d.dxtype 'diagtype_id'
    FROM opd.opd s
    INNER JOIN opd.odiag d on s.regdate = d.regdate and s.hn = d.hn and s.frequency =  d.frequency
    WHERE s.regdate = '${HisHimproHiModel.DateToYYYYMMDD(dateServe)}'
    and s.hn = '${hn}' and s.docno = '${seq}'
    UNION
    SELECT '${seq}' as 'seq',
    s.regdate 'date_serv',s.timesend as 'time_serv',
    d.diag 'icd_code',d.descrip 'icd_name',d.dxtype 'diag_type',
    '' as 'DiagNote',d.dxtype 'diagtype_id'
            FROM ipd.ipd s
            INNER JOIN ipd.idiag d on s.an = d.an
            inner join opd.opd o on s.regdate = o.regdate and s.hn = o.hn and s.frequency = o.frequency
            WHERE s.regdate = '${HisHimproHiModel.DateToYYYYMMDD(dateServe)}'
            and s.hn = '${hn}'
            and o.docno = '${seq}'
            GROUP BY s.regdate,s.an`);
    return data[0];
  }
  async getDrugs(db: Knex, hn: any, dateServe: any, seq: any) {

    let cmd = `SELECT '${seq}' AS 'seq',o.regdate 'date_serv',time_order 'time_serv',
	REPLACE (namedrug,'"','') 'drug_name',amount 'qty',i.UnitName as 'unit',
  concat(l.Line1,' ' ,l.Line2 ,' ',l.Line3) AS 'usage_line1',
	'' as 'usage_line2',
	'' as 'usage_line3'
  FROM opd.drug_order_opd d
  inner join opd.opd o on d.regdate = o.regdate and d.frequency = o.frequency and d.hn = o.hn
  inner join hos.itemlist i on i.itemcode=d.codedrug
  inner join hos.itemlabel l on d.item_usage = l.code
  WHERE d.regdate= '${HisHimproHiModel.DateToYYYYMMDD(dateServe)}'
  AND o.hn='${hn}'
  AND o.docno = '${seq}'
    UNION
    SELECT '${seq}' AS 'seq',d.orderdate 'date_serv',time_order 'time_serv',
    REPLACE (namedrug,'"','') 'drug_name',amount 'qty',i.UnitName as 'unit',
      concat(l.Line1,' ' ,l.Line2 ,' ',l.Line3) AS 'usage_line1',
      '' as 'usage_line2',
      '' as 'usage_line3'
    FROM ipd.drug_order_ipd d
    inner JOIN ipd.ipd ii ON ii.an=d.an
    inner join hos.itemlist i on i.itemcode=d.codedrug
    inner join hos.itemlabel l on d.item_usage = l.code
    inner join opd.opd o on ii.regdate = o.regdate and ii.frequency = o.frequency and ii.hn = o.hn
    WHERE ii.regdate='${HisHimproHiModel.DateToYYYYMMDD(dateServe)}'
    AND o.docno = '${seq}'
    AND ii.hn='${hn}'`;
    let data = await db.raw(cmd); //Edit 2022-08-30
    console.log("getDrugs : " + cmd);
    return data[0];
  }
  async getLabs(db: Knex, hn: any, dateServe: any, seq: any) {
    let cmd = `SELECT s.regdate 'date_serv',ifnull(lo.time_order,'00:00:00') 'time_serv',
    lo.namelab as 'labgroup',
    l.labname 'lab_name',l.result_lab 'lab_result',l.unit as 'unit',ifnull(l.normal_lab,'') 'standard_result'
   FROM opd.opd s
    inner join opd.lab_order_opd lo on s.regdate = lo.regdate and s.hn = lo.hn and lo.frequency = s.frequency
    inner JOIN opd.result_lab_opd l on s.regdate = l.regdate and s.hn = l.hn and l.frequency = s.frequency
    WHERE s.regdate = '${HisHimproHiModel.DateToYYYYMMDD(dateServe)}'
    and s.hn = '${hn}'
    and s.docno = '${seq}'
    and lo.codelab !=''
    and l.labcode !=''
    UNION
    SELECT s.regdate 'date_serv',ifnull(lo.time_order,'00:00:00') 'time_serv',
    lo.namelab as 'labgroup',
    l.labname 'lab_name',l.result_lab 'lab_result',l.unit as 'unit',ifnull(l.normal_lab,'') 'standard_result'
    FROM ipd.ipd s
    inner join ipd.lab_order_ipd lo on s.an = lo.an
    inner JOIN ipd.result_lab_ipd l on lo.an = l.an and lo.orderno = l.orderno
    inner join opd.opd o on s.regdate = o.regdate and s.frequency = o.frequency and s.hn = o.hn
    WHERE s.regdate = '${HisHimproHiModel.DateToYYYYMMDD(dateServe)}'
    and s.hn = '${hn}'
    and o.docno = '${seq}'
    and lo.codelab !=''
    and l.labcode !='' limit 400;`;
    let data = await db.raw(cmd);
    console.log("getLabs : " + cmd);
    return data[0];
  }
  async getProcedure(db: Knex, hn: any, dateServe: any, seq: any) {

    let cmd = `SELECT s.hn 'pid','${seq}' as 'seq',s.regdate 'date_serv',
    o.timein 'time_serv',o.oper 'procedure_code',o.descrip 'procedure_name',
    o.regdate 'start_date',o.timein 'start_time',o.regdate 'end_date',o.timeout 'end_time'
    FROM opd.opd s
    INNER JOIN opd.ooper o on s.regdate = o.regdate and s.hn = o.hn and s.frequency = o.frequency
    WHERE s.hn = '${hn}'
    and s.regdate = '${HisHimproHiModel.DateToYYYYMMDD(dateServe)}'
    and s.docno = '${seq}'
    UNION
    SELECT s.hn 'pid','${seq}' as 'seq',s.regdate 'date_serv',
    o.start_time 'time_serv',o.oper 'procedure_code',o.descrip 'procedure_name',
    o.startdate 'start_date',o.start_time,o.enddate 'end_date',o.end_time
    FROM ipd.ipd s
    INNER JOIN ipd.ioper o on s.an = o.an
    inner join opd.opd op on s.regdate = op.regdate and s.frequency = op.frequency and s.hn = op.hn
    WHERE s.hn = '${hn}'
    and op.docno = '${seq}'
    and s.regdate = '${HisHimproHiModel.DateToYYYYMMDD(dateServe)}';`;
    //and s.frequency = SeqSmartReferToFrequency('${seq}') `;
    let data = await db.raw(cmd);
    console.log("getProcedure : " + cmd);
    return data[0];
  }
  async getNurture(db: Knex, hn: any, dateServe: any, seq: any) {
    let cmd = `SELECT '${seq}' as 'seq',o.regdate 'date_serv',o.timereg 'time_serv',
    (SELECT p.blood_group
    FROM pcu.person p
    WHERE o.hn = p.hn limit 1)'bloodgrp',
    if(o.typebw='WPT2',ROUND(o.weight / 1000,1),o.weight) as weight,
    o.high 'height',
    o.bmi,o.temper 'temperature',o.pulse 'pr',o.respiration 'rr',
    o.hpressure 'sbp',o.lpressure 'dbp',o.sign 'symptom',o.scrroom 'depcode',
    hos.getRoomName(o.scrroom) 'department',
    '' as 'movement_score','' as 'vocal_score','' as 'eye_score','' as 'oxygen_sat','' as 'gak_coma_sco',
    o.dxtext 'diag_text','' as 'pupil_right','' as 'pupil_left'
    FROM opd.opd o
    WHERE o.regdate = '${HisHimproHiModel.DateToYYYYMMDD(dateServe)}'
    and o.hn = '${hn}'
    and o.docno = '${seq}'`;
    // INNER JOIN opd.opd o on s.regdate = o.regdate and s.hn = o.hn and s.frequency = o.frequency
    let data = await db.raw(cmd);
    console.log("getNurture : " + cmd);
    return data[0];
  }
  async getPhysical(db: Knex, hn: any, dateServe: any, seq: any) {
    let cmd = `SELECT '${seq}' as 'seq',o.invest as pe
    FROM opd o
    WHERE o.regdate = '${HisHimproHiModel.DateToYYYYMMDD(dateServe)}'
    and o.hn = '${hn}'
    and o.docno = '${seq}' `;
    let data = await db.raw(cmd);
    console.log("getPhysical : " + cmd);
    return data[0];
  }
  async getPillness(db: Knex, hn: any, dateServe: any, seq: any) {
    let cmd = `SELECT '${seq}' as 'seq',replace(i.pi,'\r\n','') as 'hpi'
    FROM opd o
    INNER JOIN opd.opd_pi i  on o.regdate = i.regdate and o.hn = i.hn and o.frequency = i.frequency
    WHERE o.regdate = '${HisHimproHiModel.DateToYYYYMMDD(dateServe)}'
    and o.hn = '${hn}'
    and o.docno = '${seq}'
    group by o.regdate,o.hn,o.frequency limit 1`;
    let data = await db.raw(cmd);
    console.log("getPillness : " + cmd);
    if (data.length > 0) {
      return data[0];
    } else {
      return null;
    }
  }
  async getXray(db: Knex, hn: any, dateServe: any, seq: any) {
    let cmd = `SELECT s.regdate 'xray_date',x.namexray 'xray_name'
    FROM opd.opd s
    INNER JOIN opd.xray_order_opd x on s.regdate = x.regdate and s.hn = x.hn and s.frequency = x.frequency
    WHERE s.regdate = '${HisHimproHiModel.DateToYYYYMMDD(dateServe)}'
    and s.hn = '${hn}'
    and s.docno = '${seq}'
    group by s.regdate,s.hn
    UNION
    SELECT x.orderdate 'xray_date',x.namexray 'xray_name'
    FROM ipd.ipd s
    INNER JOIN ipd.xray_order_ipd x on s.an = x.an
    INNER JOIN opd.opd o on s.regdate = o.regdate and s.hn = o.hn and s.frequency = o.frequency
    WHERE s.regdate = '${HisHimproHiModel.DateToYYYYMMDD(dateServe)}'
    and s.hn = '${hn}'
    and o.docno = '${seq}'
    group by s.regdate,s.hn`;
    //and s.frequency = SeqSmartReferToFrequency('${seq}')
    let data = await db.raw(cmd);
    console.log("getXray : " + cmd);
    return data[0];
  }
  static DateToYYYYMMDD(Date: Date): String {
    let DS: string = Date.getFullYear()
      + '-' + ('0' + (Date.getMonth() + 1)).slice(-2)
      + '-' + ('0' + Date.getDate()).slice(-2);
    return DS;
  }
}
