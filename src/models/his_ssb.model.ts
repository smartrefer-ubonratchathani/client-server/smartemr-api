import { Knex } from 'knex';
export class HisSsbModel {
    async getLogin(db: Knex, username: any, password: any) {
        let data = await db.raw(`
        SELECT Code AS username, Memo AS password, SUBSTRING(LocalName, 2, 200) AS fullname, 10686 AS hcode, Memo
        FROM dbo.DNSYSCONFIG
        WHERE (CtrlCode = '10031') AND (LocalName IS NOT NULL) AND
        Code like '${username}' AND Memo like '${password}'
        `);
        return data;
    }
    async getVisit(db: Knex, startDate: any, endDtae: any) {
        let data = await db.raw(`
        SELECT 
dbo.HNOPD_MASTER.VN AS seq, dbo.HNOPD_MASTER.HN as hn,
dbo.GEN_PrefixName.pname,
RIGHT(dbo.HNPAT_NAME.FirstName,LEN(dbo.HNPAT_NAME.FirstName) - 1) AS first_name,
RIGHT(dbo.HNPAT_NAME.LastName, LEN(dbo.HNPAT_NAME.LastName) - 1) AS last_name, 
dbo.GEN_Clinic.ClinicName AS department, 
CONVERT(varchar, dbo.HNOPD_MASTER.VisitDate, 23) AS date_serv,
CONVERT(varchar, dbo.HNOPD_PRESCRIP.NurseReleaseDateTime, 108) AS time_serv
FROM 
dbo.HNOPD_MASTER  
LEFT JOIN
dbo.HNOPD_PRESCRIP ON dbo.HNOPD_MASTER.VisitDate = dbo.HNOPD_PRESCRIP.VisitDate AND 
dbo.HNOPD_MASTER.VN = dbo.HNOPD_PRESCRIP.VN LEFT JOIN
dbo.HNPAT_NAME ON dbo.HNOPD_MASTER.HN = dbo.HNPAT_NAME.HN LEFT JOIN
dbo.GEN_PrefixName ON dbo.HNPAT_NAME.InitialNameCode = dbo.GEN_PrefixName.pname_id LEFT JOIN
dbo.GEN_Clinic ON dbo.HNOPD_PRESCRIP.Clinic = dbo.GEN_Clinic.ClinicID
WHERE 
(dbo.HNPAT_NAME.SuffixSmall = 0) 
and CONVERT(varchar, dbo.HNOPD_MASTER.VisitDate, 23) between'${startDate}' and '${endDtae}'
and dbo.HNOPD_PRESCRIP.CloseVisitCode = '012'

UNION

SELECT 
dbo.HNOPD_MASTER.VN AS seq, dbo.HNOPD_MASTER.HN as hn,
dbo.GEN_PrefixName.pname,
RIGHT(dbo.HNPAT_NAME.FirstName,LEN(dbo.HNPAT_NAME.FirstName) - 1) AS first_name,
RIGHT(dbo.HNPAT_NAME.LastName, LEN(dbo.HNPAT_NAME.LastName) - 1) AS last_name, 
dbo.GEN_Ward.WardName AS department, 
CONVERT(varchar, dbo.HNIPD_MASTER.DischargeDateTime, 23) AS date_serv,
CONVERT(varchar, dbo.HNIPD_MASTER.DischargeDateTime, 108) AS time_serv

FROM 
dbo.HNIPD_MASTER     
LEFT JOIN
dbo.HNOPD_MASTER ON  dbo.HNIPD_MASTER.HN = dbo.HNOPD_MASTER.HN and  dbo.HNIPD_MASTER .AN = dbo.HNOPD_MASTER.AN   
LEFT JOIN
dbo.HNPAT_NAME ON dbo.HNOPD_MASTER.HN = dbo.HNPAT_NAME.HN 
LEFT JOIN
dbo.GEN_PrefixName ON dbo.HNPAT_NAME.InitialNameCode = dbo.GEN_PrefixName.pname_id 
LEFT JOIN
dbo.GEN_Ward ON dbo.HNIPD_MASTER.ActiveWard = dbo.GEN_Ward.WardID
WHERE 
(dbo.HNPAT_NAME.SuffixSmall = 0) 
and CONVERT(varchar, dbo.HNIPD_MASTER.DischargeDateTime, 23) between'${startDate}' and '${endDtae}'
`);
        return data;
    }
    async getServices(db: Knex, hn: any, seq: any) {
        let data = await db.raw(`
        SELECT 
dbo.HNOPD_MASTER.VN AS seq, dbo.HNOPD_MASTER.HN as hn,
dbo.GEN_PrefixName.pname,
RIGHT(dbo.HNPAT_NAME.FirstName,LEN(dbo.HNPAT_NAME.FirstName) - 1) AS first_name,
RIGHT(dbo.HNPAT_NAME.LastName, LEN(dbo.HNPAT_NAME.LastName) - 1) AS last_name, 
dbo.GEN_Clinic.ClinicName AS department, 
CONVERT(varchar, dbo.HNOPD_MASTER.VisitDate, 23) AS date_serv,
CONVERT(varchar, dbo.HNOPD_PRESCRIP.NurseReleaseDateTime, 108) AS time_serv
FROM 
dbo.HNOPD_MASTER  
LEFT JOIN
dbo.HNOPD_PRESCRIP ON dbo.HNOPD_MASTER.VisitDate = dbo.HNOPD_PRESCRIP.VisitDate AND 
dbo.HNOPD_MASTER.VN = dbo.HNOPD_PRESCRIP.VN LEFT JOIN
dbo.HNPAT_NAME ON dbo.HNOPD_MASTER.HN = dbo.HNPAT_NAME.HN LEFT JOIN
dbo.GEN_PrefixName ON dbo.HNPAT_NAME.InitialNameCode = dbo.GEN_PrefixName.pname_id LEFT JOIN
dbo.GEN_Clinic ON dbo.HNOPD_PRESCRIP.Clinic = dbo.GEN_Clinic.ClinicID
WHERE 
(dbo.HNPAT_NAME.SuffixSmall = 0) 
and dbo.HNOPD_MASTER.HN = '${hn}' 
and dbo.HNOPD_MASTER.VN = '${seq}' 
and dbo.HNOPD_PRESCRIP.CloseVisitCode = '012'

UNION

SELECT 
dbo.HNOPD_MASTER.VN AS seq, dbo.HNOPD_MASTER.HN as hn,
dbo.GEN_PrefixName.pname,
RIGHT(dbo.HNPAT_NAME.FirstName,LEN(dbo.HNPAT_NAME.FirstName) - 1) AS first_name,
RIGHT(dbo.HNPAT_NAME.LastName, LEN(dbo.HNPAT_NAME.LastName) - 1) AS last_name, 
dbo.GEN_Ward.WardName AS department, 
CONVERT(varchar, dbo.HNIPD_MASTER.DischargeDateTime, 23) AS date_serv,
CONVERT(varchar, dbo.HNIPD_MASTER.DischargeDateTime, 108) AS time_serv

FROM 
dbo.HNIPD_MASTER     
LEFT JOIN
dbo.HNOPD_MASTER ON  dbo.HNIPD_MASTER.HN = dbo.HNOPD_MASTER.HN and  dbo.HNIPD_MASTER .AN = dbo.HNOPD_MASTER.AN   
LEFT JOIN
dbo.HNPAT_NAME ON dbo.HNOPD_MASTER.HN = dbo.HNPAT_NAME.HN 
LEFT JOIN
dbo.GEN_PrefixName ON dbo.HNPAT_NAME.InitialNameCode = dbo.GEN_PrefixName.pname_id 
LEFT JOIN
dbo.GEN_Ward ON dbo.HNIPD_MASTER.ActiveWard = dbo.GEN_Ward.WardID
WHERE 
(dbo.HNPAT_NAME.SuffixSmall = 0) 
and dbo.HNOPD_MASTER.HN = '${hn}' 
and dbo.HNOPD_MASTER.VN = '${seq}'
`);
        return data;
    }
    async getProfile(db: Knex, hn: any, seq: any) {
        let data = await db.raw(`
        SELECT  dbo.HNOPD_MASTER.vn ,dbo.HNPAT_INFO.HN as hn,CASE dbo.HNPAT_INFO.Gender WHEN 1 THEN 'หญิง' WHEN 2 THEN 'ชาย' END as sex, dbo.HNPAT_REF.RefNo AS cid, dbo.GEN_PrefixName.pname AS title_name, 
RIGHT(dbo.HNPAT_NAME.FirstName, LEN(dbo.HNPAT_NAME.FirstName) - 1) AS first_name, 
        RIGHT(dbo.HNPAT_NAME.LastName, LEN(dbo.HNPAT_NAME.LastName) - 1) AS last_name, dbo.HNPAT_ADDRESS.Address1 AS addrpart, dbo.HNPAT_ADDRESS.Moo as moo, SUBSTRING(dbo.HNPAT_ADDRESS.ProvinceComposeCode, 
        7, 2) AS tmbpart, SUBSTRING(dbo.HNPAT_ADDRESS.ProvinceComposeCode, 4, 2) AS amppart
       , SUBSTRING(dbo.HNPAT_ADDRESS.ProvinceComposeCode, 1, 2) AS chwpart
       , CONVERT(varchar, dbo.HNPAT_INFO.BirthDateTime, 23) AS brthdate
       , dbo.get_Exact_Date_diff(dbo.HNPAT_INFO.BirthDateTime, { fn NOW() }) AS age , dbo.Gen_Occupation.OccupationName as occupation,
        dbo.HNOPD_RIGHT.RightCode AS pttype_id, dbo.GEN_Right.RightName AS pttype_name, NULL AS pttype_no, 
        dbo.HNOPD_RIGHT.MainHospital AS hospmain, dbo.GEN_Hospital.HospName AS hospmain_name, dbo.HNOPD_RIGHT.SubHospital AS hospsub, dbo.GEN_Hospital1.HospName AS hospsub_name, CONVERT(varchar, 
        dbo.HNPAT_INFO.MakeDateTime, 23) AS registdate, NULL AS visitdate, RIGHT(dbo.HNPAT_REFERNAME.FirstName, LEN(dbo.HNPAT_REFERNAME.FirstName) - 1) AS father_name, RIGHT(HNPAT_REFERNAME_1.FirstName, 
                         LEN(HNPAT_REFERNAME_1.FirstName) - 1) AS mother_name, RIGHT(HNPAT_REFERNAME_2.FirstName, LEN(HNPAT_REFERNAME_2.FirstName) - 1) AS couple_name, dbo.GEN_NotifiedPerson.NotifiedPerson AS contact_name, 
                         dbo.GEN_NotifiedPerson.RelativeName AS contact_relation, dbo.GEN_NotifiedPerson.CommunicableNo AS contact_mobile
        FROM  dbo.HNPAT_INFO 
             INNER JOIN dbo.HNOPD_MASTER on dbo.HNPAT_INFO.HN = dbo.HNOPD_MASTER.HN 
             INNER JOIN dbo.HNOPD_RIGHT on dbo.HNOPD_RIGHT.VN =dbo.HNOPD_MASTER.VN AND dbo.HNOPD_MASTER.VisitDate = dbo.HNOPD_RIGHT.VisitDate
INNER JOIN
                         dbo.HNPAT_NAME ON dbo.HNPAT_INFO.HN = dbo.HNPAT_NAME.HN INNER JOIN
                         dbo.HNPAT_REF ON dbo.HNPAT_INFO.HN = dbo.HNPAT_REF.HN AND (dbo.HNPAT_REF.RefNoType = 'ID') left JOIN
                         dbo.GEN_PrefixName ON dbo.HNPAT_NAME.InitialNameCode = dbo.GEN_PrefixName.pname_id left JOIN
                         dbo.GEN_Right ON dbo.HNOPD_RIGHT.RightCode = dbo.GEN_Right.RightCode left JOIN
                         dbo.HNPAT_REFERNAME ON dbo.HNPAT_INFO.HN = dbo.HNPAT_REFERNAME.HN AND (dbo.HNPAT_REFERNAME.HNPatReferenceNameType = 1) left JOIN
                         dbo.HNPAT_REFERNAME AS HNPAT_REFERNAME_1 ON dbo.HNPAT_INFO.HN = HNPAT_REFERNAME_1.HN AND (HNPAT_REFERNAME_1.HNPatReferenceNameType = 2) left JOIN
                         dbo.HNPAT_REFERNAME AS HNPAT_REFERNAME_2 ON dbo.HNPAT_INFO.HN = HNPAT_REFERNAME_2.HN AND (HNPAT_REFERNAME_2.HNPatReferenceNameType = 3) left JOIN
                         dbo.GEN_NotifiedPerson ON dbo.HNPAT_INFO.HN = dbo.GEN_NotifiedPerson.HN LEFT JOIN
                         dbo.GEN_Hospital1 ON dbo.HNOPD_RIGHT.SubHospital = dbo.GEN_Hospital1.HospCode LEFT  JOIN
                         dbo.GEN_Hospital ON dbo.HNOPD_RIGHT.MainHospital = dbo.GEN_Hospital.HospCode LEFT JOIN
                         dbo.HNPAT_ADDRESS ON dbo.HNPAT_NAME.HN = dbo.HNPAT_ADDRESS.HN
LEFT JOIN dbo.Gen_Occupation ON dbo.HNPAT_INFO.Occupation = dbo.Gen_Occupation.OccupationCode
WHERE 
(dbo.HNPAT_NAME.SuffixSmall = 0) 
AND (dbo.HNPAT_ADDRESS.SuffixTiny = 1) 
and dbo.HNPAT_INFO.HN = '${hn}'
and dbo.HNOPD_MASTER.VN = '${seq}'`);
        return data;
    }
    async getHospital(db: Knex, hn: any) {
        let data = await db.raw(`
        SELECT TOP (1) Code AS provider_code, RIGHT(LocalName, LEN(LocalName) - CHARINDEX('/', LocalName)) + RIGHT(LEFT(LocalName, CHARINDEX('/', LocalName + '/') - 1), LEN(LEFT(LocalName, CHARINDEX('/', LocalName + '/') - 1)) - 1) AS provider_name
        FROM dbo.DNSYSCONFIG
        WHERE (CtrlCode = '42025') AND (Code <> '') AND (Code <> '00000') and Code ='10686'
        ORDER BY provider_code`);
        return data;
    }
    async getAllergyDetail(db: Knex, hn: any) {
        let data = await db.raw(`
        SELECT  
        dbo.HNPAT_ALLERGIC.HN, 
        RIGHT(dbo.PNK_STOCKMASTER.LocalName, LEN(dbo.PNK_STOCKMASTER.LocalName) - 1) AS drug_name,
        dbo.HNPAT_ALLERGIC.RemarksMemo AS symptom, NULL AS begin_date, 
        dbo.HNPAT_ALLERGIC.MakeDateTime AS daterecord
        FROM 
        dbo.HNPAT_ALLERGIC 
        LEFT JOIN dbo.PNK_STOCKMASTER ON dbo.HNPAT_ALLERGIC.StockCode = dbo.PNK_STOCKMASTER.StockCode 
        WHERE 
        (dbo.HNPAT_ALLERGIC.InactiveDate IS NULL) and 
        dbo.HNPAT_ALLERGIC.HN = '${hn}'`);
        return data;
    }
    async getDiagnosis(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        SELECT distinct 
dbo.HNOPD_MASTER.VN AS seq, 
CONVERT(varchar,dbo.HNOPD_MASTER.VisitDate, 23) AS date_serv, 
CONVERT(varchar,dbo.HNOPD_MASTER.VisitDate, 108) AS time_serv, 
dbo.HNOPD_PRESCRIP_DIAG.ICDCode AS icd_code,
RIGHT(dbo.HNICD_MASTER.EnglishName,LEN(dbo.HNICD_MASTER.EnglishName) - 1) AS icd_name, 
dbo.HNOPD_PRESCRIP_DIAG.DiagnosisRecordType  AS diag_type, 
dbo.HNOPD_PRESCRIP_DIAG.DiagThaiMemo AS DiagNote, 
dbo.HNOPD_PRESCRIP_DIAG.DiagnosisRecordType AS diagtype_id
FROM    
dbo.HNOPD_MASTER INNER JOIN 
dbo.HNOPD_PRESCRIP_DIAG ON dbo.HNOPD_MASTER.VisitDate = dbo.HNOPD_PRESCRIP_DIAG.VisitDate AND 
dbo.HNOPD_MASTER.VN = dbo.HNOPD_PRESCRIP_DIAG.VN
INNER JOIN  
dbo.HNICD_MASTER ON dbo.HNICD_MASTER.IcdCode = dbo.HNOPD_PRESCRIP_DIAG.ICDCode 
where 
dbo.HNOPD_MASTER.VN = '${seq}' and dbo.HNOPD_MASTER.VisitDate='${dateServe}' 
union 
SELECT dbo.HNOPD_MASTER.VN As seq, 
CONVERT(varchar, dbo.HNIPD_DIAG.DiagDateTime, 23) AS date_serv, 
CONVERT(varchar, dbo.HNIPD_DIAG.DiagDateTime, 108) AS time_serv, 
dbo.HNIPD_DIAG.ICDCode AS icd_code,
RIGHT(dbo.HNICD_MASTER.EnglishName,LEN(dbo.HNICD_MASTER.EnglishName) - 1) AS icd_name, 
dbo.HNIPD_DIAG.DiagnosisRecordType  AS diag_type , 
dbo.HNIPD_DIAG.DiagThaiMemo AS DiagNote, 
dbo.HNIPD_DIAG.DiagnosisRecordType AS diagtype_id
FROM dbo.HNIPD_DIAG 
INNER JOIN
dbo.HNICD_MASTER ON dbo.HNIPD_DIAG.ICDCode = dbo.HNICD_MASTER.IcdCode 
inner join 
dbo.HNOPD_MASTER on dbo.HNIPD_DIAG.AN = dbo.HNOPD_MASTER.AN
where 
dbo.HNOPD_MASTER.VN = '${seq}' and dbo.HNOPD_MASTER.VisitDate='${dateServe}' 
ORDER BY diag_type ASC
        `);
        return data;
    }
    async getDrugs(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        SELECT  dbo.HNOPD_MASTER.VN AS seq, 
        CONVERT(varchar, dbo.HNOPD_MASTER.VisitDate, 23) AS date_serv, 
        CONVERT(varchar, dbo.HNOPD_MASTER.VisitDate, 108) AS time_serv, 
        RIGHT(dbo.PNK_STOCKMASTER.LocalName, LEN(dbo.PNK_STOCKMASTER.LocalName) - 1) AS drug_name, 
        dbo.HNOPD_PRESCRIP_MEDICINE.Qty AS qty, 
        dbo.GEN_Unit.UnitName AS unit, 
        dbo.HNOPD_PRESCRIP_MEDICINE.DoseMemo AS usage_line1,
        NULL AS usage_line2, 
        NULL AS usage_line3
        FROM   dbo.HNOPD_MASTER
        inner join dbo.HNOPD_PRESCRIP_MEDICINE on dbo.HNOPD_PRESCRIP_MEDICINE.vn = dbo.HNOPD_MASTER.vn and dbo.HNOPD_PRESCRIP_MEDICINE.VisitDate = '${dateServe}'
        LEFT OUTER JOIN dbo.GEN_Unit ON dbo.HNOPD_PRESCRIP_MEDICINE.UnitCode = dbo.GEN_Unit.UnitID 
        LEFT OUTER JOIN dbo.PNK_STOCKMASTER ON dbo.HNOPD_PRESCRIP_MEDICINE.StockCode = dbo.PNK_STOCKMASTER.StockCode
        where dbo.HNOPD_MASTER.VN ='${seq}' and CONVERT(varchar, dbo.HNOPD_MASTER.VisitDate, 23) ='${dateServe}'
        union
        SELECT dbo.HNOPD_MASTER.vn as seq,
        CONVERT(varchar, dbo.HNIPD_DRUG_HISTORY.IPDChargeDateTime, 23) AS date_serv, 
        CONVERT(varchar, dbo.HNIPD_DRUG_HISTORY.IPDChargeDateTime, 108) AS time_serv, 
        RIGHT(dbo.PNK_STOCKMASTER.LocalName, LEN(dbo.PNK_STOCKMASTER.LocalName) - 1) AS drug_name, 
        dbo.HNIPD_DRUG_HISTORY.Qty AS qty, dbo.GEN_Unit.UnitName AS unit, 
        dbo.HNIPD_DRUG_HISTORY.DoseMemo AS usage_line1, 
        NULL AS usage_line2, 
        NULL AS usage_line3
        FROM  dbo.HNOPD_MASTER          
        inner join dbo.HNIPD_DRUG_HISTORY  on dbo.HNIPD_DRUG_HISTORY.AN = dbo.HNOPD_MASTER.AN and CONVERT(varchar, dbo.HNIPD_DRUG_HISTORY.IPDChargeDateTime, 23) = '${dateServe}'
        LEFT OUTER JOIN dbo.GEN_Unit ON dbo.HNIPD_DRUG_HISTORY.UnitCode = dbo.GEN_Unit.UnitID 
        LEFT OUTER JOIN dbo.PNK_STOCKMASTER ON dbo.HNIPD_DRUG_HISTORY.StockCode = dbo.PNK_STOCKMASTER.StockCode 
        where 
        dbo.HNOPD_MASTER.vn ='${seq}' 
        and CONVERT(varchar, dbo.HNOPD_MASTER.VisitDate, 23) ='${dateServe}'`);
        return data;
    }
    async getLabs(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        SELECT HNOPD_MASTER.VN AS vn, 
CONVERT (varchar, dbo.HNLABREQ_RESULT.ApproveDateTime, 23)  AS date_serv,
CONVERT (varchar, dbo.HNLABREQ_RESULT.ApproveDateTime, 108)  AS time_serv,
dbo.GEN_Facility_Rooms.FacilityRoomsName AS labgroup,
dbo.GEN_LabItem.LabName AS lab_name, 
dbo.HNLABREQ_RESULT.ResultValue AS lab_result, 
dbo.HNLABREQ_RESULT.NormaltResultValue AS standard_result,
NULL AS unit 
FROM       
        dbo.HNOPD_MASTER inner join  dbo.HNLABREQ_RESULT on dbo.HNOPD_MASTER.HN = dbo.HNLABREQ_RESULT.HN LEFT JOIN
        dbo.GEN_LabItem ON dbo.HNLABREQ_RESULT.LabCode = dbo.GEN_LabItem.LabCode LEFT JOIN
        dbo.GEN_Facility_Rooms ON dbo.HNLABREQ_RESULT.FacilityRmsNo = dbo.GEN_Facility_Rooms.FacilityRoomsCode 

WHERE  
    CONVERT(DATE,dbo.HNLABREQ_RESULT.ApproveDateTime) IN 
    ( SELECT TOP (1)  CONVERT(DATE,dbo.HNLABREQ_RESULT.ApproveDateTime) FROM  dbo.HNOPD_MASTER LEFT JOIN 
     dbo.HNLABREQ_RESULT on dbo.HNOPD_MASTER.HN = dbo.HNLABREQ_RESULT.HN              
          WHERE HNOPD_MASTER.VN ='${seq}' and HNOPD_MASTER.VisitDate = '${dateServe}' 
          ORDER BY dbo.HNLABREQ_RESULT.ApproveDateTime DESC ) and 
    HNOPD_MASTER.VN ='${seq}' and HNOPD_MASTER.VisitDate = '${dateServe}' 
    AND dbo.HNLABREQ_RESULT.LabCode NOT IN ('I0016','I001601','I001602','I0017','I001701','I001702','I001703','I0018','I001801',
     'I001802','I001803','I001804','I001805','I001806','I0019','I001901','I001902','I0038','I003801','I003802','I003803','OL0276','OL027601','OL027602')
    ORDER BY dbo.HNLABREQ_RESULT.ApproveDateTime DESC`);
        return data;
    }
    async getProcedure(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        SELECT  dbo.HNOPD_MASTER.VN AS seq, 
CONVERT(varchar, dbo.HNOPD_MASTER.VisitDate, 23) AS date_serv, 
CONVERT(varchar, dbo.HNOPD_PRESCRIP.MakeDateTime, 108) AS time_serv, 
dbo.HNOPD_PRESCRIP_TREATMENT.TreatmentCode AS procedure_code, 
dbo.GEN_Treatment.TreatmentName AS procedure_name,
       CONVERT(varchar, dbo.HNOPD_PRESCRIP_TREATMENT.MakeDateTime, 23) AS start_date, 
                         CONVERT(varchar, dbo.HNOPD_PRESCRIP_TREATMENT.MakeDateTime, 108) AS start_time, 
       NULL AS end_date, 
       NULL AS end_time
FROM            dbo.HNOPD_PRESCRIP_TREATMENT INNER JOIN
                         dbo.HNOPD_PRESCRIP ON dbo.HNOPD_PRESCRIP_TREATMENT.VisitDate = dbo.HNOPD_PRESCRIP.VisitDate AND dbo.HNOPD_PRESCRIP_TREATMENT.VN = dbo.HNOPD_PRESCRIP.VN AND 
                         dbo.HNOPD_PRESCRIP_TREATMENT.PrescriptionNo = dbo.HNOPD_PRESCRIP.PrescriptionNo INNER JOIN
                         dbo.HNOPD_MASTER ON dbo.HNOPD_PRESCRIP.VisitDate = dbo.HNOPD_MASTER.VisitDate AND dbo.HNOPD_PRESCRIP.VN = dbo.HNOPD_MASTER.VN INNER JOIN
                         dbo.GEN_Treatment ON dbo.HNOPD_PRESCRIP_TREATMENT.TreatmentCode = dbo.GEN_Treatment.TreatmentCode
WHERE        (dbo.HNOPD_PRESCRIP_TREATMENT.CxlDateTime IS NULL) AND (dbo.HNOPD_PRESCRIP_TREATMENT.TreatmentCode IS NOT NULL)
and dbo.HNOPD_MASTER.VN = '${seq}' and dbo.HNOPD_MASTER.VisitDate = '${dateServe}'
union 
select 
dbo.HNOPD_MASTER.VN AS seq,
CONVERT(varchar, dbo.HNIPD_CHARGE.IPDChargeDateTime, 23) AS date_serv, 
CONVERT(varchar, dbo.HNIPD_CHARGE.IPDChargeDateTime, 108) AS time_serv, 
dbo.HNIPD_CHARGE.TreatmentCode as procedure_code,
dbo.GEN_Treatment.TreatmentName as procedure_name,
CONVERT(varchar, dbo.HNIPD_CHARGE.IPDChargeDateTime, 23) AS start_date, 
CONVERT(varchar, dbo.HNIPD_CHARGE.IPDChargeDateTime, 108) AS start_time,
NULL as end_date,
NULL as end_time
from 
dbo.HNIPD_MASTER
inner join dbo.HNOPD_MASTER on dbo.HNIPD_MASTER.AN = dbo.HNOPD_MASTER.AN
inner join dbo.HNIPD_CHARGE on dbo.HNIPD_MASTER.AN = dbo.HNIPD_CHARGE.AN 
inner join dbo.GEN_Treatment on dbo.HNIPD_CHARGE.TreatmentCode = dbo.GEN_Treatment.TreatmentCode
where 
dbo.HNOPD_MASTER.VN = '${seq}' and dbo.HNOPD_MASTER.VisitDate = '${dateServe}';
        `);
        return data;
    }
    async getNurture(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        SELECT TOP 1                        
dbo.HNOPD_MASTER.VN AS seq, 
CONVERT(varchar, dbo.HNOPD_MASTER.VisitDate, 23) AS date_serv, 
CONVERT(varchar, dbo.HNOPD_MASTER.VisitDate, 108) AS time_serv, 
CASE dbo.HNPAT_INFO.BloodGroupType
     WHEN 0 THEN 'None'
        WHEN 1 THEN 'O' 
        WHEN 2 THEN 'A' 
        WHEN 3 THEN 'B'
        WHEN 4 THEN 'AB'
        WHEN 5 THEN 'Non Specified'END  AS bloodgrp,
dbo.HNOPD_VITALSIGN.BodyWeight AS weight, 
dbo.HNOPD_VITALSIGN.Height, dbo.HNOPD_VITALSIGN.BMIValue AS bmi, 
dbo.HNOPD_VITALSIGN.Temperature, 
dbo.HNOPD_VITALSIGN.PulseRate AS pr, 
dbo.HNOPD_VITALSIGN.RespirationRate AS rr, 
dbo.HNOPD_VITALSIGN.BpSystolic AS sbp, 
dbo.HNOPD_VITALSIGN.BpDiastolic AS dbp, 
dbo.HNOPD_VITALSIGN.Remarks AS cc, 
dbo.HNOPD_PRESCRIP.Clinic AS depcode, 
dbo.GEN_Clinic.ClinicName AS department, 
NULL AS movement_score, 
NULL AS vocal_score, 
NULL AS eye_score, 
dbo.HNOPD_VITALSIGN.O2Sat AS oxygen_sat, 
NULL AS gak_coma_sco, 
NULL AS diag_text, 
dbo.HNOPD_VITALSIGN.RightPupilSizeType AS pupil_right, 
dbo.HNOPD_VITALSIGN.LeftPupilSizeType AS pupil_left
FROM 
dbo.HNOPD_MASTER           
left JOIN dbo.HNOPD_VITALSIGN ON dbo.HNOPD_MASTER.VisitDate = dbo.HNOPD_VITALSIGN.VisitDate AND 
dbo.HNOPD_MASTER.VN = dbo.HNOPD_VITALSIGN.VN left JOIN
dbo.HNOPD_PRESCRIP ON dbo.HNOPD_VITALSIGN.VisitDate = dbo.HNOPD_PRESCRIP.VisitDate AND 
dbo.HNOPD_VITALSIGN.VN = dbo.HNOPD_PRESCRIP.VN left JOIN
dbo.HNPAT_INFO ON dbo.HNOPD_MASTER.HN = dbo.HNPAT_INFO.HN left JOIN
dbo.GEN_Clinic ON dbo.HNOPD_PRESCRIP.Clinic = dbo.GEN_Clinic.ClinicID
where 
dbo.HNOPD_MASTER.VN = '${seq}' and 
dbo.HNOPD_MASTER.VisitDate = '${dateServe}'
order by CONVERT(varchar, dbo.HNOPD_VITALSIGN.EntryDateTime, 108) desc
        `);
        return data;
    }
    async getPhysical(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        SELECT       
        dbo.HNOPD_MASTER.VN AS seq, 
        dbo.HNOPD_VITALSIGN.Remarks AS hpi
        FROM dbo.HNOPD_MASTER          
        LEFT JOIN  dbo.HNOPD_VITALSIGN  on dbo.HNOPD_MASTER.VN = dbo.HNOPD_VITALSIGN.VN 
        where 
        dbo.HNOPD_MASTER.VN = '${seq}' and 
        dbo.HNOPD_MASTER.VisitDate = '${dateServe}'
        and dbo.HNOPD_VITALSIGN.VisitDate = '${dateServe}'
        and (dbo.HNOPD_VITALSIGN.Remarks IS NOT NULL)
        `);
        return data;
    }
    async getPillness(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        SELECT       dbo.HNOPD_MASTER.VN AS seq, RemarksMemo AS hpi
        FROM            dbo.HNOPD_PRESCRIP 
        inner join dbo.HNOPD_MASTER on dbo.HNOPD_MASTER.VN = dbo.HNOPD_PRESCRIP.VN 
        where 
        dbo.HNOPD_MASTER.VN = '${seq}' and 
        dbo.HNOPD_MASTER.VisitDate = '${dateServe}'
        and dbo.HNOPD_PRESCRIP.VisitDate = '${dateServe}'
        and RemarksMemo is not null
        `);
        return data;
    }
    async getXray(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        SELECT dbo.HNOPD_MASTER.VN AS seq, dbo.HNXRAYREQ_ITEM.EntryDateTime AS xray_date, dbo.GEN_Xray.XrayName AS xray_name
        FROM           
        dbo.HNOPD_MASTER inner join 
        dbo.HNXRAYREQ_HEADER on dbo.HNOPD_MASTER.HN = dbo.HNXRAYREQ_HEADER.HN INNER JOIN
        dbo.HNXRAYREQ_ITEM ON dbo.HNXRAYREQ_HEADER.FacilityRmsNo = dbo.HNXRAYREQ_ITEM.FacilityRmsNo AND dbo.HNXRAYREQ_HEADER.RequestNo = dbo.HNXRAYREQ_ITEM.RequestNo INNER JOIN
        dbo.GEN_Xray ON dbo.HNXRAYREQ_ITEM.XrayCode = dbo.GEN_Xray.XrayCode
        WHERE (dbo.HNXRAYREQ_ITEM.CxlReasonCode IS NULL)
        and dbo.HNOPD_MASTER.VN = '${seq}' and 
        dbo.HNOPD_MASTER.VisitDate = '${dateServe}'
        and CONVERT(varchar,dbo.HNXRAYREQ_HEADER.EntryDateTime,23) = '${dateServe}'
        `);
        return data;
    }
}
