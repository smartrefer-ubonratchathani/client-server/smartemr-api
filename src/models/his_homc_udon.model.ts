import { Knex } from 'knex';
const hospcode = process.env.API_HIS_CODE;
export class HisHomcUbonModel {
  async getLogin(db: Knex, username: any, password: any) {
    let data = await db.raw(`
    SELECT RTRIM(LTRIM(profile.UserCode)) as username ,CONCAT(RTRIM(LTRIM(profile.firstName)),' ',RTRIM(LTRIM(profile.lastName))) as fullname, '10671' as hcode
    from profile
    WHERE RTRIM(LTRIM(profile.UserCode)) = '${username}'
    and profile.PWDUdon = '${password}'
    `);
    return data;
  }
  async getHospital(db: Knex, hn: any) {
    let data = await db.raw(`SELECT OFF_ID as provider_code,rtrim(NAME) as provider_name from HOSPCODE where OFF_ID = '${hospcode}'`);
    return data;
  }
  async getProfile(db: Knex, hn: any, seq: any) {
    let data = await db.raw(`SELECT TOP 1 ltrim(rtrim(p.hn)) as hn,rtrim(t.titleName) as title_name, rtrim(p.firstName) as first_name, rtrim(p.lastName) as last_name, rtrim(ps.CardID) AS cid
    , p.moo as moopart, (RTRIM(LTRIM(p.addr1)) + '  ' + RTRIM(LTRIM(p.addr2))) as addrpart, p.tambonCode as tmbpart
    , SUBSTRING(p.regionCode, 3, 2) as amppart, p.areaCode as chwpart
    ,convert(date,convert(char,p.birthDay -5430000)) as brthdate
    ,(case when right(p.birthDay,4) = '0000' then RIGHT('000'+LTRIM(STR(year(getdate())+543 - left(p.birthDay,4))),3)+'-00-00'
    else  RIGHT('000'+LTRIM(STR(year(getdate())+543 - year(p.birthDay))),3) + '-'+ 
    RIGHT('00'+LTRIM(STR(ABS(DATEDIFF(month, p.birthDay, getdate()))%12)),2)+'-'+ 
    RIGHT('00'+LTRIM(STR(replace(DATEDIFF(DAY,DAY(p.birthDay),DAY(GETDATE())),'-',''))),2) end) as age 
    , i.useDrg AS pttype_id,rtrim(pt.pay_typedes) as  pttype_name, RTRIM(LTRIM(ps.SocialID)) as pttype_no
    , i.HMAIN AS hospmain, h1.NAME as hospmain_name, i.HSUB AS hospsub, h2.NAME as hospsub_name
    ,convert(date,convert(char,o.registDate -5430000))  as registDate
    ,(select Top 1 convert(date,convert(char,VisitDate -5430000))   from PATDIAG with(nolock) where Hn=p.hn group by Hn,VisitDate order by VisitDate desc) as VisitDate 
     ,ps.father as father_name 
    ,p.mother as mother_name
    ,'' as couple_name
    ,ps.relatives as contact_name
    ,ps.relationship as contact_relation
    ,'' as contact_mobile
    ,(case when p.sex='ช' then 'ชาย' else 'หญิง' end) as sex
    ,(select occdes from Occup where p.occupation = occcode) as occupation
    FROM PATIENT p
    LEFT OUTER JOIN PTITLE t ON p.titleCode=t.titleCode
    LEFT OUTER JOIN PatSS ps ON p.hn = ps.hn
    LEFT OUTER JOIN OPD_H o ON p.hn = o.hn
    LEFT OUTER JOIN Bill_h i ON o.hn = i.hn AND o.regNo = i.regNo
    LEFT OUTER JOIN Paytype pt ON i.useDrg = pt.pay_typecode 
    LEFT OUTER JOIN DEPT d ON o.dept = d.deptCode
    LEFT OUTER JOIN HOSPCODE h1 ON i.HMAIN = h1.OFF_ID
    LEFT OUTER JOIN HOSPCODE h2 ON i.HSUB = h2.OFF_ID
    WHERE p.hn=dbo.padc('${hn}',' ',7)
    ORDER BY o.registDate DESC`);
    return data;
  }
  async getAllergyDetail(db: Knex, hn: any) {
    let data = await db.raw(`
    select v.gen_name as drug_name,m.alergyNote as symptom 
    ,convert(date,left(dbo.ymd2cymd(dbo.ce2ymd(m.updDate)),4)+'-'+right(left(dbo.ymd2cymd(dbo.ce2ymd(m.updDate)),6),2)+'-'+right(dbo.ymd2cymd(dbo.ce2ymd(m.updDate)),2)) as begin_date
    ,convert(date,left(dbo.ymd2cymd(dbo.ce2ymd(m.updTime)),4)+'-'+right(left(dbo.ymd2cymd(dbo.ce2ymd(m.updTime)),6),2)+'-'+right(dbo.ymd2cymd(dbo.ce2ymd(m.updTime)),2)) as daterecord
    from medalery m left join Med_inv v on m.medCode=v.abbr where m.hn=dbo.padc('${hn}',' ',7) order by m.updDate desc
    `);
    return data;
  }
  async getVisit(db: Knex, startDate: any, endDtae: any) {
    let data = await db.raw(`SELECT ltrim(rtrim(o.hn)) + o.regNo as seq,dbo.padc(o.hn,' ',7) as hn, convert(date,convert(char,o.registDate -5430000)) as date_serv
    ,left(o.timePt,2)+':'+right (o.timePt,2) as time_serv, d.deptDesc as department 
    from OPD_H as o 
    left join DEPT d on(d.deptCode = o.dept) 
    left join Refer r on o.hn=r.Hn 
    where convert(date,convert(char,o.registDate -5430000)) between'${startDate}' and '${endDtae}'
    union all 
    SELECT i.hn+i.regist_flag as seq,dbo.padc(i.hn,' ',7) as hn
    ,convert(date,convert(char,i.admit_date -5430000)) as date_serv
    ,left(i.admit_time,2)+':'+right(i.admit_time,2) as time_serv
    ,w.ward_name as department
    from Ipd_h as i
    left join Ward w on w.ward_id = i.ward_id
    left join Refer r on i.hn = r.Hn
    where convert(date,convert(char,i.admit_date -5430000)) between'${startDate}' and '${endDtae}'
    `);
    return data;
  }
  async getServices(db: Knex, hn: any, seq: any) {
    let data = await db.raw(`SELECT ltrim(rtrim(o.hn)) + o.regNo as seq,dbo.padc(o.hn,' ',7) as hn, convert(date,convert(char,o.registDate -5430000)) as date_serv
    ,left(o.timePt,2)+':'+right (o.timePt,2) as time_serv, d.deptDesc as department 
    from OPD_H as o 
    left join DEPT d on(d.deptCode = o.dept) 
    left join Refer r on o.hn=r.Hn 
    where o.hn = dbo.padc('${hn}',' ',7) and o.regNo =  right('${seq}' ,2)
    union all 
    SELECT i.hn+i.regist_flag as seq,dbo.padc(i.hn,' ',7) as hn
    ,convert(date,convert(char,i.admit_date -5430000)) as date_serv
    ,left(i.admit_time,2)+':'+right(i.admit_time,2) as time_serv
    ,w.ward_name as department
    from Ipd_h as i
    left join Ward w on w.ward_id = i.ward_id
    left join Refer r on i.hn = r.Hn
    where i.hn = dbo.padc('${hn}',' ',7) and i.regist_flag = right('${seq}' ,2)
    `);
    return data;
  }
  async getDiagnosis(db: Knex, hn: any, dateServe: any, vn: any) {
    let data = await db.raw(`
    select ltrim(rtrim(r.Hn)) + r.RegNo as seq
    , convert(date, convert(char, r.ReferDate - 5430000)) as date_serv
    , '' as time_serv
    , substring(r.ReferDiag,1,charindex(':',r.ReferDiag)-1) as icd_code
    , substring(r.ReferDiag,charindex(':',r.ReferDiag)+1,len(r.ReferDiag)) as icd_name
    , '1' as diag_type
    , rtrim(r.ReferDiag) as DiagNote
    , '1' as diagtype_id
    from Refer r
    left join Ipd_h i on r.Hn = i.hn and r.RegNo = i.regist_flag
    where r.ReferStatus <> 'I' 
    and r.Hn = dbo.padc('${hn}',' ',7) and r.RegNo = right('${vn}', 2) 
    `);
    return data;
  }
  async getProcedure(db: Knex, hn: any, dateServe: any, vn: any) {
    let data = await db.raw(`select ltrim(rtrim(p.hn)) + o.REGNO as seq,
      (o.OR_DATE) as date_serv, o.OR_TIME as time_serv, o.ORCODE as procedure_code, o.ORDESC as icd_name,
      (o.START_DATE) as start_date, (o.END_DATE) as end_date
    from ORREQ_H o
    left join OPD_H p on(o.HN = p.hn and o.REGNO = p.regNo)
    where o.HN = dbo.padc('${hn}',' ',7) and o.REGNO = right('${vn}', 2) 
    order by date_serv desc `);
    return data;
  }
  async getDrugs(db: Knex, hn: any, dateServe: any, vn: any) {
    let data = await db.raw(` select x.*
    from (select DISTINCT
    ltrim(rtrim(pm.hn)) + pm.registNo as seq
      , convert(date, convert(char, h.registDate - 5430000)) as date_serv
      , SUBSTRING(h.timePt, 1, 2) + ':' + SUBSTRING(h.timePt, 3, 4) as time_serv
      , m.name as drug_name
      , RTRIM(pm.accAmt) as qty
      , RTRIM(pm.unit) as unit
      , RTRIM(pm.lamedTimeText) + ' ' + RTRIM(pm.lamedText) as usage_line1
      , '' as usage_line2
      , '' as usage_line3
    from Patmed pm(NOLOCK)
    left join OPD_H h(NOLOCK) on(h.hn = pm.hn and  h.regNo = pm.registNo)
    left join Med_inv m(NOLOCK)  on(pm.invCode = m.code and m.site = '1')
    where pm.hn = dbo.padc('${hn}',' ',7)
    and h.ipdStatus = '0'
    AND   pm.registNo = right('${vn}', 2)
    union all
    select DISTINCT
    i.hn + i.regist_flag as seq
      , convert(date, convert(char, i.admit_date - 5430000)) as date_serv
      , left(i.admit_time, 2) + ':' + right(i.admit_time, 2) as time_serv
      , m.name as drug_name
      , pm.accAmt as qty
      , m.unit as unit
      , RTRIM(pm.lamedTimeText) + ' ' + RTRIM(pm.lamedText) as usage_line1
      , '' as usage_line2
      , '' as usage_line3
    from Patmed pm(NOLOCK)
    left join Ipd_h i(NOLOCK) on(i.hn = pm.hn and  i.regist_flag = pm.registNo)
    left join Med_inv m(NOLOCK)  on(pm.invCode = m.code and m.site = '1')
    where pm.hn = dbo.padc('${hn}',' ',7)  and i.ladmit_n != ''
    AND i.regist_flag = right('${vn}', 2) ) as x 
    order by x.date_serv desc,x.time_serv desc
    `);
    return data;
  }
  async getLabs(db: Knex, hn: any, dateServe: any, vn: any) {
    let data = await db.raw(` 
    select
    convert(date, convert(char, a.res_date - 5430000)) as date_serv
      , left(a.res_time, 2) + ':' + right(a.res_time, 2) as time_serv
      , isnull(a.lab_code, '') as labgroup
      , a.result_name as lab_name
      , isnull(replace(replace(a.real_res,'""',''),'!',''),'รอผล') as lab_result
      , isnull(s.result_unit,'') as unit
      , a.low_normal + '-' + a.high_normal as standard_result
    from Labres_d a(nolock)
    left join OPD_H o(nolock) on(o.hn = a.hn and o.regNo = a.reg_flag)
    left join Labreq_h b(nolock) on(a.req_no = b.req_no)
    left join Labre_s s(nolock) on(a.lab_code = s.lab_code and a.res_item = s.res_run_no and s.labType = a.lab_type)
    LEFT JOIN Lab lb ON(a.lab_code = lb.labCode and lb.labType = a.lab_type)
    left join Ward w on(w.ward_id = b.reqFrom)
    left join PatSS ss on(ss.hn = a.hn)
    where a.hn = dbo.padc('${hn}',' ',7) AND a.real_res<>''
    AND a.reg_flag = right('${vn}', 2)
    order by a.res_date asc ,left(a.res_time, 2) + ':' + right(a.res_time, 2) desc
    `);
    //console.log(data);

    return data;
  }
  async getNurture(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
    select top 1 ltrim(rtrim(v.hn)) + v.RegNo as seq
    ,convert(date, convert(char, v.VisitDate)) as date_serv
    ,CONVERT(VARCHAR(5),v.VisitDate, 108)  AS time_serv
    ,isnull((select PATIENT.bloodGroup from PATIENT where PATIENT.bloodGroup <> '' and PATIENT.hn = v.hn),'') as bloodgrp
    ,v.Weight AS weight, v.Height as height
    ,v.BMI as bmi
    ,v.Temperature AS temperature
    ,v.Pulse AS pr
    ,v.Breathe AS rr
    ,v.Hbloodpress as sbp
    ,v.Lbloodpress AS dbp
    ,isnull(SUBSTRING(v.Symtom,1,(case when CHARINDEX('PI:', v.Symtom) <= 0 then  100 else  CHARINDEX('PI:', v.Symtom) end)-1),'') as symptom
    ,v.deptCode as depcode
    ,(select deptDesc from DEPT d where d.deptCode = v.deptCode) as department
    ,v.ER_M as movement_score
    ,v.ER_V as vocal_score
    ,v.ER_E as eye_score
    ,v.ER_O2sat as oxygen_sat
    ,v.ER_Rt as pupil_right
    ,v.ER_Lt as pupil_left
    ,CONVERT(INT, v.ER_M)+CONVERT(INT, v.ER_V)+CONVERT(INT, v.ER_E) as gak_coma_sco
    ,isnull(ReferDiag,'') as diag_text
    from VitalSign v
    left join Refer as r on v.hn=r.Hn and v.RegNo=r.RegNo and r.ReferStatus <> 'I'
    left join PATDIAG dx on(v.hn=dx.Hn and v.RegNo=dx.regNo)
    where v.hn= dbo.padc('${hn}',' ',7) and v.RegNo = right('${seq}', 2)
    order by v.VitalSignNo desc
    `);
    return data;
  }
  async getPhysical(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
    SELECT ltrim(rtrim(OPD_H.hn)) + PATDIAG.regNo AS seq
    , '' as pe
    FROM OPD_H
    INNER JOIN PATDIAG ON OPD_H.hn = PATDIAG.Hn and OPD_H.regNo = PATDIAG.regNo
    INNER JOIN ICD101 ON PATDIAG.ICDCode = ICD101.CODE
    INNER JOIN DOCC ON PATDIAG.DocCode = DOCC.docCode
    left outer JOIN VitalSign ON OPD_H.hn = VitalSign.hn and OPD_H.regNo = VitalSign.RegNo
    WHERE OPD_H.hn = dbo.padc('${hn}',' ',7) AND PATDIAG.regNo = right('${seq}', 2) AND (PATDIAG.pt_status = 'O')
    order by PATDIAG.regNo desc

      `);
    return data;
  }
  async getPillness(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`SELECT
    ltrim(rtrim(OPD_H.hn)) + PATDIAG.regNo AS seq
      , SUBSTRING(Symtom,isnull(CHARINDEX('PI:', Symtom),0),150) as hpi
    FROM OPD_H
    INNER JOIN PATDIAG ON OPD_H.hn = PATDIAG.Hn and OPD_H.regNo = PATDIAG.regNo
    INNER JOIN ICD101 ON PATDIAG.ICDCode = ICD101.CODE
    INNER JOIN DOCC ON PATDIAG.DocCode = DOCC.docCode
    left outer JOIN VitalSign ON OPD_H.hn = VitalSign.hn and OPD_H.regNo = VitalSign.RegNo
    WHERE OPD_H.hn = dbo.padc('${hn}',' ',7) AND PATDIAG.regNo = right('${seq}', 2) AND (PATDIAG.pt_status = 'O')
    order by PATDIAG.regNo desc
      `);
    return data;
  }
  async getXray(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
          select convert(date, convert(char, x.xreq_date - 5430000)) as xray_date,c.xproc_des as xray_name
          from XresHis x 
          left join OPD_H o (NOLOCK) on (o.hn = x.hn and o.regNo = x.regist_flag) 
          LEFT JOIN Xreq_h xh on(xh.hn = x.hn and xh.regist_flag = x.regist_flag and x.xrunno = xh.xrunno) 
          LEFT JOIN Xpart p ON(x.xpart_code = p.xpart_code) 
          LEFT JOIN Xreq_d d ON(x.xprefix = d.xprefix AND x.xrunno = d.xrunno) 
          LEFT JOIN Xcon xc on(x.xprefix = xc.xprefix) 
          LEFT JOIN Xproc c ON(d.xpart_code = c.xproc_part  AND d.xproc_code = c.xproc_code) 
          WHERE xc.xchr_code <> 'PAT' 
          AND x.hn = dbo.padc('${hn}',' ',7)
          AND x.regist_flag = right('${seq}', 2)
          order by  x.xreq_date DESC
         `);
    return data;
  }
}
