import { Knex } from 'knex';
export class HisUbaseModel {
  async getLogin(db: Knex, username: any, password: any) {
    let data = await db.raw(`
         SELECT a.CID as username , a.cid as fullname,'15078' as hcode   
         from  staff a
         WHERE a.CID = '${username}' 
         and SUBSTRING(a.CID, 10, 13) = '${password}'
         `);
    return data[0];
  }
  async getVisit(db: Knex, startDate: any, endDtae: any) {
    let data = await db.raw(`
    SELECT DISTINCT
    o.VISIT_ID as seq,o.hn as hn,
		CASE 
             WHEN p.PRENAME not in('') THEN p.PRENAME
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW())< '20' AND p.sex='1' AND p.MARRIAGE = '4'THEN 'สามเณร'
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '20' AND p.sex='1' AND p.MARRIAGE  = '4'THEN 'พระภิกษุ'
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) < '15'  AND p.sex='1' THEN 'เด็กชาย'
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '15' AND p.sex='1' THEN 'นาย'
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) < '15'  AND p.sex='2' THEN 'เด็กหญิง'
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '15' AND p.sex='2' AND p.MARRIAGE ='1' THEN 'นางสาว'
             ELSE 'นาง' 
		END AS title_name 
    ,p.fname as first_name,p.lname as last_name, 
    DATE_FORMAT(date(r.RF_DT),'%Y-%m-%d') as date_serv, 
		TIME_FORMAT(time(r.RF_DT),'%h:%i:%s') as time_serv,  
    d.UNIT_NAME as department
    FROM refers r
		LEFT JOIN opd_visits o ON r.VISIT_ID = o.VISIT_ID AND o.IS_CANCEL = 0
    LEFT JOIN cid_hn b ON o.hn = b.hn
    LEFT JOIN population p ON b.cid = p.cid 
    LEFT JOIN service_units d ON o.unit_reg = d.unit_id
    WHERE DATE_FORMAT(date(r.RF_DT),'%Y-%m-%d') between'${startDate}' and '${endDtae}'
	AND r.IS_CANCEL = 0 
	AND r.VISIT_ID NOT IN (SELECT visit_id FROM ipd_reg where is_cancel = 0) 
	LIMIT 1       
		UNION
	SELECT 
	i.VISIT_ID as seq,b.hn as hn,
		CASE 
             WHEN p.PRENAME not in('') THEN p.PRENAME
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW())< '20' AND p.sex='1' AND p.MARRIAGE = '4'THEN 'สามเณร'
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '20' AND p.sex='1' AND p.MARRIAGE  = '4'THEN 'พระภิกษุ'
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) < '15'  AND p.sex='1' THEN 'เด็กชาย'
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '15' AND p.sex='1' THEN 'นาย'
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) < '15'  AND p.sex='2' THEN 'เด็กหญิง'
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '15' AND p.sex='2' AND p.MARRIAGE ='1' THEN 'นางสาว'
             ELSE 'นาง' 
		END AS title_name 
,p.fname as first_name,p.lname as last_name, 
DATE_FORMAT(date(r.RF_DT),'%Y-%m-%d') as date_serv, 
TIME_FORMAT(time(r.RF_DT),'%h:%i:%s') as time_serv, 
    e.UNIT_NAME as department
from refers r 
LEFT  JOIN ipd_reg i ON r.VISIT_ID = i.VISIT_ID AND i.IS_CANCEL = 0
LEFT  JOIN opd_visits b ON i.VISIT_ID = b.VISIT_ID AND b.IS_CANCEL = 0
LEFT  JOIN cid_hn c ON b.hn = c.hn 
LEFT JOIN population p ON c.cid = p.cid
LEFT  JOIN service_units e ON i.WARD_NO = e.UNIT_ID
AND r.IS_CANCEL = 0
WHERE DATE_FORMAT(date(r.RF_DT),'%Y-%m-%d') between'${startDate}' and '${endDtae}'
AND r.IS_CANCEL = 0 LIMIT 1
   `);
    return data[0];
  }
  async getServices(db: Knex, hn: any, seq: any) {
    let data = await db.raw(`
    SELECT DISTINCT
    o.VISIT_ID as seq,o.hn as hn,
		CASE 
             WHEN p.PRENAME not in('') THEN p.PRENAME
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW())< '20' AND p.sex='1' AND p.MARRIAGE = '4'THEN 'สามเณร'
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '20' AND p.sex='1' AND p.MARRIAGE  = '4'THEN 'พระภิกษุ'
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) < '15'  AND p.sex='1' THEN 'เด็กชาย'
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '15' AND p.sex='1' THEN 'นาย'
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) < '15'  AND p.sex='2' THEN 'เด็กหญิง'
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '15' AND p.sex='2' AND p.MARRIAGE ='1' THEN 'นางสาว'
             ELSE 'นาง' 
		END AS title_name 
    ,p.fname as first_name,p.lname as last_name, 
    DATE_FORMAT(date(r.RF_DT),'%Y-%m-%d') as date_serv, 
		TIME_FORMAT(time(r.RF_DT),'%h:%i:%s') as time_serv,  
    d.UNIT_NAME as department
    FROM refers r
		LEFT JOIN opd_visits o ON r.VISIT_ID = o.VISIT_ID AND o.IS_CANCEL = 0
    LEFT JOIN cid_hn b ON o.hn = b.hn
    LEFT JOIN population p ON b.cid = p.cid 
    LEFT JOIN service_units d ON o.unit_reg = d.unit_id
    WHERE  o.hn ='${hn}' and r.visit_id = '${seq}'
	AND r.IS_CANCEL = 0 
	AND r.VISIT_ID NOT IN (SELECT visit_id FROM ipd_reg where is_cancel = 0) 
	LIMIT 1       
		UNION
	SELECT 
	i.VISIT_ID as seq,b.hn as hn,
		CASE 
             WHEN p.PRENAME not in('') THEN p.PRENAME
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW())< '20' AND p.sex='1' AND p.MARRIAGE = '4'THEN 'สามเณร'
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '20' AND p.sex='1' AND p.MARRIAGE  = '4'THEN 'พระภิกษุ'
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) < '15'  AND p.sex='1' THEN 'เด็กชาย'
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '15' AND p.sex='1' THEN 'นาย'
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) < '15'  AND p.sex='2' THEN 'เด็กหญิง'
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '15' AND p.sex='2' AND p.MARRIAGE ='1' THEN 'นางสาว'
             ELSE 'นาง' 
		END AS title_name 
,p.fname as first_name,p.lname as last_name, 
DATE_FORMAT(date(r.RF_DT),'%Y-%m-%d') as date_serv, 
TIME_FORMAT(time(r.RF_DT),'%h:%i:%s') as time_serv, 
    e.UNIT_NAME as department
from refers r 
LEFT  JOIN ipd_reg i ON r.VISIT_ID = i.VISIT_ID AND i.IS_CANCEL = 0
LEFT  JOIN opd_visits b ON i.VISIT_ID = b.VISIT_ID AND b.IS_CANCEL = 0
LEFT  JOIN cid_hn c ON b.hn = c.hn 
LEFT JOIN population p ON c.cid = p.cid
LEFT  JOIN service_units e ON i.WARD_NO = e.UNIT_ID
AND r.IS_CANCEL = 0
WHERE  b.hn ='${hn}' and r.visit_id = '${seq}'
AND r.IS_CANCEL = 0 LIMIT 1
   `);
    return data[0];
  }
  async getProfile(db: Knex, hn: any, seq: any) {
    let data = await db.raw(`
    select o.HN as hn, 
    p.cid as cid,
   CASE 
 WHEN PRENAME not in('') THEN PRENAME
 WHEN TIMESTAMPDIFF(year,p.birthdate,NOW()) < '20' AND p.sex='1' AND p.MARRIAGE = '4'THEN 'สามเณร'
 WHEN TIMESTAMPDIFF(year,p.birthdate,NOW()) >= '20' AND p.sex='1' AND p.MARRIAGE = '4'THEN 'พระภิกษุ'
 WHEN TIMESTAMPDIFF(year,p.birthdate,NOW()) < '15'  AND p.sex='1' THEN 'เด็กชาย'
 WHEN TIMESTAMPDIFF(year,p.birthdate,NOW()) >= '15' AND p.sex='1' THEN 'นาย'
 WHEN TIMESTAMPDIFF(year,p.birthdate,NOW()) < '15'  AND p.sex='2' THEN 'เด็กหญิง'
 WHEN TIMESTAMPDIFF(year,p.birthdate,NOW()) >= '15' AND p.sex='2' AND p.MARRIAGE='1' THEN 'นางสาว'
ELSE 'นาง'
END AS title_name
   , p.FNAME as first_name
   , p.LNAME as last_name
   , SUBSTR(p.TOWN_ID,7,2) as moopart
   , trim(p.HOME_ADR) as addrpart
   , SUBSTR(p.TOWN_ID,5,2) as tmbpart
   , SUBSTR(p.TOWN_ID,3,2) as amppart
   , SUBSTR(p.TOWN_ID,1,2) as chwpart
   , p.BIRTHDATE as brthdate
   , concat(lpad(timestampdiff(year,p.birthdate,curdate()),3,'0'),'-'
  ,lpad(timestampdiff(month,p.birthdate,curdate())-(timestampdiff(year,p.birthdate,curdate())*12),2,'0'),'-'
  ,lpad(timestampdiff(day,date_add(p.birthdate,interval (timestampdiff(month,p.birthdate,curdate())) month),curdate()),2,'0')) as age
   , CASE
     WHEN p.SEX = 1 THEN 'ชาย'
     WHEN p.SEX = 2 THEN 'หญิง'
     END as sex
   , replace(replace(oc.oc_name,char(13),''),char(10),'') as occupation
   , m.INSCL as pttype_id
    , m.INSCL_NAME as pttype_name
    , g.uc_cardid as pttype_no
    , g.HOSPMAIN  as hospmain
    , hop.HOSP_NAME as hospmain_name
    , g.HOSPSUB as hospsub
    , hs.HOSP_NAME as hospsub_name
    , o.REG_DATETIME as registdate
    , max(date(o.REG_DATETIME)) as visitdate
    , p.FATHER as father_name
    , p.MOTHER as mother_name
    ,p.TELEPHONE as contact_mobile
    ,'' as couple_name
   , p.contact as contact_name
   , rl.RL_NAME as contact_relation
FROM population as p 
 INNER JOIN cid_hn c on p.CID = c.CID
INNER JOIN opd_visits o ON c.HN = o.HN
 LEFT JOIN towns t ON p.town_id = t.town_id
 LEFT  JOIN main_inscls m ON p.INSCL = m.INSCL
 LEFT JOIN uc_inscl g ON p.CID = g.CID AND (g.date_abort >= date(o.REG_DATETIME) or day(g.date_abort)=0) 
 LEFT JOIN hospitals hop ON g.HOSPMAIN = hop.HOSP_ID
 LEFT JOIN occupations oc ON p.oc_id = oc.oc_id
 LEFT JOIN hospitals hs ON g.HOSPSUB = hs.HOSP_ID
   LEFT JOIN relatives rl ON p.RL_ID = rl.RL_ID
 WHERE o.hn =  '${hn}'
        `);
    return data[0];
  }
  async getHospital(db: Knex, hn: any) {
    let data = await db.raw(`
        SELECT a.HOSP_ID as provider_code,a.HOSP_NAME as provider_name 
        FROM hospitals a
        where a.HOSP_ID = '15078'`);
    return data[0];
  }
  async getAllergyDetail(db: Knex, hn: any) {
    let data = await db.raw(`
        SELECT  replace(b.DRUG_NAME,'\r\n','') as drug_name,
        replace(replace(a.ALLERGY_NOTE,char(13),''),char(10),'') as symptom ,
        a.ADRDATE as begin_date,
        date(a.UPD_DT) as daterecord
        FROM cid_drug_allergy a
        INNER JOIN drugs b ON a.DRUG_ID = b.DRUG_ID
        INNER JOIN cid_hn c ON a.cid = c.cid 
        WHERE hn ='${hn}'
    `);
    return data[0];
  }
  async getPillness(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
    SELECT o.VISIT_ID as seq ,
    replace(replace(replace(o.history,char(13),''),char(10),''),'""','') as hpi 
    FROM opd_visits o WHERE  o.visit_id = '${seq}' group by o.visit_id
    `);
    return data[0];
  }
  async getPhysical(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
  SELECT o.VISIT_ID as seq ,
   replace(replace(replace(o.pexam,char(13),''),char(10),''),'""','') as pe 
   FROM opd_visits o WHERE  o.visit_id = '${seq}' group by o.visit_id
   `);
    return data[0];
  }
  async getNurture(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
    SELECT o.VISIT_ID as seq,
    DATE_FORMAT(date(o.REG_DATETIME),'%Y-%m-%d') as date_serv, 
    TIME_FORMAT(time(o.REG_DATETIME),'%h:%i:%s') as time_serv, 
    bl.BG_NAME as bloodgrp,
    o.WEIGHT as weight,
    o.HEIGHT as height,
     CAST((o.WEIGHT/ POWER(o.HEIGHT / 100,2)) AS DECIMAL(4,2)) as bmi,
    o.BODY_TEMP as temperature,
    o.PULSE_RATE as pr,
    o.RESP_RATE as rr,
    o.BP_SYST as sbp,
    o.BP_DIAS as dbp,
    replace(replace(replace(SUBSTR(o.history,LOCATE('CHIEF COMPLAINT',o.history),
    LOCATE('PRESENT ILLNESS',o.history)- 
    LOCATE('CHIEF COMPLAINT',o.history)) ,char(13),''),char(10),''),'""','')  as symptom,
    s.UNIT_ID as depcode,
    s.UNIT_NAME as department,
    v.GCS_M as movement_score,
		v.GCS_V as vocal_score,
		v.GCS_E as eye_score,
		v.oxygen as oxygen_sat,
		v.GCS_M + v.GCS_V + v.GCS_E as gak_coma_sco,	 
    replace(i.ICD_NAME,'\r\n','') as diag_text,
    '' as pupil_right,
	  '' as pupil_left
    FROM opd_visits  o 
    LEFT JOIN service_units s ON o.unit_reg = s.unit_id
    LEFT JOIN cid_hn c ON o.hn = c.hn
    LEFT JOIN population p ON c.cid = p.cid
    LEFT JOIN blood_group bl ON p.bl_group = bl.BG_ID
    LEFT JOIN v_signs v ON o.VISIT_ID = v.VISIT_ID
    LEFT JOIN opd_diagnosis od ON o.VISIT_ID = od.VISIT_ID
    LEFT JOIN icd10new i ON od.ICD10 = i.ICD10
        WHERE o.VISIT_ID ='${seq}'
	LIMIT 0 , 1
    `);
    return data[0];
  }
  async getDiagnosis(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
      SELECT a.VISIT_ID as seq,
       date_format(a.REG_DATETIME,'%Y-%m-%d') as date_serv,
       time(a.REG_DATETIME) as time_serv,
       c.code as icd_code,
      IF(c.ICD_NAME!='', c.ICD_NAME, c.NICKNAME) as icd_name,
       b.DXT_ID as diag_type, '' as DiagNote, b.DXT_ID  as diagtype_id
      FROM opd_visits a
      INNER JOIN opd_diagnosis b ON a.VISIT_ID = b.VISIT_ID AND b.IS_CANCEL = 0
      INNER JOIN icd10new c ON b.ICD10 = c.ICD10
      WHERE a.visit_id ='${seq}'
    `);
    return data[0];
  }
  async getProcedure(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
      SELECT DISTINCT
                  a.hn as pid,
                  a.VISIT_ID AS seq,
                  DATE(a.REG_DATETIME) AS date_serv,
                  TIME(a.REG_DATETIME) AS time_serv,
                  c. CODE AS procedure_code,
                  c.TNAME AS procedure_name,
                  DATE(b.OP_DT) AS start_date,
         TIME(b.OP_DT) AS start_time,
                 IF(DATE(b.OP_END)!='0000-00-00', DATE(b.OP_END), DATE(b.OP_DT)) AS end_date,
         '0000-00-00' AS end_time
              FROM
                  opd_visits a
              INNER JOIN opd_operations b ON a.VISIT_ID = b.VISIT_ID AND b.IS_CANCEL = 0
              INNER JOIN icd9cm c ON b.icd9 = c.ICD9
              WHERE a.VISIT_ID = '${seq}'
      `);
    return data[0];
  }
  async getDrugs(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
    SELECT a.VISIT_ID as seq,
    DATE_FORMAT(date(a.REG_DATETIME),'%Y%m%d') as date_serv,
    time(a.REG_DATETIME) as time_serv,
    replace(c.DRUG_NAME,'\r\n','') as drug_name,
    b.RX_AMOUNT as qty,
    d.UUNIT_NAME as unit ,
replace(replace(replace(c.INFO_PATIENT,char(13),''),char(10),''),'""','') as usage_line1 ,
replace(replace(replace(c.DRUG_INFO,char(13),''),char(10),''),'""','') as usage_line2,
replace(replace(replace(c.ATTENTION,char(13),''),char(10),''),'""','') as usage_line3
    FROM opd_visits a
INNER JOIN prescriptions b ON a.VISIT_ID = b.VISIT_ID AND b.IS_CANCEL =0
INNER JOIN drugs c ON b.DRUG_ID = c.DRUG_ID
INNER  JOIN usage_units d ON c.PACKAGE = d.UUNIT_ID
WHERE a.VISIT_ID = '${seq}'
UNION
SELECT i.VISIT_ID as seq,
      DATE_FORMAT(date(i.adm_dt),'%Y-%m-%d') as date_serv,
      time(i.adm_dt) as time_serv,
      replace(c.DRUG_NAME,'\r\n','') as drug_name,
     b.rx_dose as qty,
     CONCAT(c.prepack,' ',d.UUNIT_NAME) as unit ,
  replace(replace(replace(c.INFO_PATIENT,char(13),''),char(10),''),'""','') as usage_line1 ,
  replace(replace(replace(c.DRUG_INFO,char(13),''),char(10),''),'""','') as usage_line2,
  replace(replace(replace(c.ATTENTION,char(13),''),char(10),''),'""','') as usage_line3
      FROM ipd_reg i
  INNER JOIN ipd_oneday b ON i.visit_id = b.visit_id AND b.is_cancel = 0
  INNER JOIN drugs c ON b.DRUG_ID = c.DRUG_ID
  INNER  JOIN usage_units d ON c.PACKAGE = d.UUNIT_ID
  WHERE i.VISIT_ID = '${seq}'
  UNION
SELECT i.VISIT_ID as seq,
      DATE_FORMAT(date(i.adm_dt),'%Y-%m-%d') as date_serv,
      time(i.adm_dt) as time_serv,
      replace(c.DRUG_NAME,'\r\n','') as drug_name,
     b.rx_dose as qty,
     CONCAT(c.prepack,' ',d.UUNIT_NAME) as unit ,
  replace(replace(replace(c.INFO_PATIENT,char(13),''),char(10),''),'""','') as usage_line1 ,
  replace(replace(replace(c.DRUG_INFO,char(13),''),char(10),''),'""','') as usage_line2,
  replace(replace(replace(c.ATTENTION,char(13),''),char(10),''),'""','') as usage_line3
      FROM ipd_reg i
  INNER JOIN ipd_cont b ON i.visit_id = b.visit_id AND b.is_cancel = 0
  INNER JOIN drugs c ON b.DRUG_ID = c.DRUG_ID
  INNER  JOIN usage_units d ON c.PACKAGE = d.UUNIT_ID
  WHERE i.VISIT_ID = '${seq}'
  `);
    return data[0];
  }
  async getLabs(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
      SELECT
      DATE_FORMAT(date(a.LREQ_DT),'%Y%m%d') as date_serv,
      DATE_FORMAT(time(a.LREQ_DT),'%h:%i:%s') as time_serv,
      b.LAB_GROUP as labgroup,
       replace(b.LAB_NAME,'\r\n','') as lab_name, 
      replace(replace(replace(trim(SUBSTR(a.LAB_RESULT, LOCATE('=', a.LAB_RESULT)+1)),char(13),''),char(10),''),'^','')  as lab_result,
    '' as unit,
    replace(b.normal_val,'\r\n','') as standard_result
      FROM lab_requests a
      INNER JOIN lab_lists b ON a.LAB_ID = b.LAB_ID AND b.IS_CANCEL =0
      WHERE a.VISIT_ID ='${seq}'
   AND a.is_cancel = 0
   AND b.is_secret = 0
  `);
    return data[0];
  }
  async getXray(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
  SELECT x.VISIT_ID as seq,
  date(x.XREQ_DATETIME) as xray_date,
  REPLACE(xl.xray_name,"\r\n"," ") as xray_name
  FROM xray_requests x
  INNER JOIN xray_pacs xl ON xl.xray_code = x.xray_code
  WHERE x.VISIT_ID = '${seq}'
  `);
    return data[0];
  }
}

