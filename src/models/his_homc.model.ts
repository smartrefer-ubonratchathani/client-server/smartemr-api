import { log } from 'console';
import { Knex } from 'knex';
var md5 = require('md5');
const hospcode = process.env.API_HIS_CODE;
export class HisHomcModel {
  async getLogin(db: Knex, username: any, password: any) {
    let pass = md5(password).toUpperCase();
    let data = await db.raw(`
    SELECT RTRIM(LTRIM(profile.UserCode)) as username ,CONCAT(RTRIM(LTRIM(profile.firstName)),' ',RTRIM(LTRIM(profile.lastName))) as fullname, '10669' as hcode 
    from profile WHERE RTRIM(LTRIM(profile.UserCode)) = '${username}' 
    and profile.PassCode = '${pass}' 
    `);
    return data;
  }
  async getHospital(db: Knex, hn: any) {
    let data = await db.raw(`SELECT OFF_ID as provider_code,rtrim(NAME) as provider_name from HOSPCODE where OFF_ID = '10669'`);
    return data;
  }
  async getProfile(db: Knex, hn: any, seq: any) {
    let data = await db.raw(`SELECT TOP 1 ltrim(rtrim(p.hn)) as hn,rtrim(t.titleName) as title_name, rtrim(p.firstName) as first_name, rtrim(p.lastName) as last_name, rtrim(ps.CardID) AS cid
    , p.moo as moopart, (RTRIM(LTRIM(p.addr1)) + '  ' + RTRIM(LTRIM(p.addr2))) as addrpart, p.tambonCode as tmbpart
    , SUBSTRING(p.regionCode, 3, 2) as amppart, p.areaCode as chwpart
    ,convert(date,convert(char, concat(substring(p.birthDay,1,4), case when substring(p.birthDay,5,2)='00' then '01' else substring(p.birthDay,5,2) end, case when substring(p.birthDay,7,2)='00' then '01' else substring(p.birthDay,7,2) end) -5430000)) as brthdate
    ,concat(abs(datediff(month, dbo.ymd2ce(p.birthDay), getdate()))/12,'-',abs(datediff(month, dbo.ymd2ce(p.birthDay), getdate()))%12,'-',abs(datediff(day, dbo.ymd2ce(p.birthDay), getdate()))%12) as age
    , i.useDrg AS pttype_id,rtrim(pt.pay_typedes) as  pttype_name, RTRIM(LTRIM(ps.SocialID)) as pttype_no
    , i.HMAIN AS hospmain, RTRIM(h1.NAME) as hospmain_name, i.HSUB AS hospsub, h2.NAME as hospsub_name
    ,convert(date,convert(char,o.registDate -5430000))  as registDate
    ,(select Top 1 convert(date,convert(char,VisitDate -5430000))  from PATDIAG with(nolock) where Hn=p.hn group by Hn,VisitDate order by VisitDate desc) as VisitDate 
     ,RTRIM(ps.father) as father_name 
    ,RTRIM(p.mother) as mother_name
    ,'' as couple_name
    ,RTRIM(ps.relatives) as contact_name
    ,RTRIM(ps.relationship) as contact_relation
    ,'' as contact_mobile
    ,(case when p.sex='ช' then 'ชาย' else 'หญิง' end) as sex
    ,(select occdes from Occup where p.occupation = occcode) as occupation
    FROM PATIENT p
    LEFT OUTER JOIN PTITLE t ON p.titleCode=t.titleCode
    LEFT OUTER JOIN PatSS ps ON p.hn = ps.hn
    LEFT OUTER JOIN OPD_H o ON p.hn = o.hn
    LEFT OUTER JOIN Bill_h i ON o.hn = i.hn AND o.regNo = i.regNo
    LEFT OUTER JOIN Paytype pt ON i.useDrg = pt.pay_typecode 
    LEFT OUTER JOIN DEPT d ON o.dept = d.deptCode
    LEFT OUTER JOIN HOSPCODE h1 ON i.HMAIN = h1.OFF_ID
    LEFT OUTER JOIN HOSPCODE h2 ON i.HSUB = h2.OFF_ID
    WHERE p.hn=dbo.padc('${hn}',' ',7)
    ORDER BY o.registDate DESC`);
    return data;
  }
  async getAllergyDetail(db: Knex, hn: any) {
    let data = await db.raw(`select RTRIM(v.gen_name) as drug_name,RTRIM(m.alergyNote) as symptom 
    ,convert(date,left(dbo.ymd2cymd(dbo.ce2ymd(m.updDate)),4)+'-'+right(left(dbo.ymd2cymd(dbo.ce2ymd(m.updDate)),6),2)+'-'+right(dbo.ymd2cymd(dbo.ce2ymd(m.updDate)),2)) as begin_date
    ,convert(date,left(dbo.ymd2cymd(dbo.ce2ymd(m.updTime)),4)+'-'+right(left(dbo.ymd2cymd(dbo.ce2ymd(m.updTime)),6),2)+'-'+right(dbo.ymd2cymd(dbo.ce2ymd(m.updTime)),2)) as daterecord
    from medalery m left join Med_inv v on m.medCode=v.abbr where m.hn=dbo.padc('${hn}',' ',7) order by m.updDate desc`);
    return data;
  }
  async getVisit(db: Knex, startDate: any, endDtae: any) {
    let data = await db.raw(`SELECT ltrim(rtrim(o.hn)) + o.regNo as seq,LTRIM(RTRIM(o.hn)) as hn, convert(date,convert(char,o.registDate -5430000)) as date_serv
    ,left(o.timePt,2)+':'+right (o.timePt,2) as time_serv, RTRIM(d.deptDesc) as department 
    from OPD_H as o 
    left join DEPT d on(d.deptCode =dbo.Firstcode (o.dept,';') ) 
    left join Bill_h b on(b.hn = o.hn and b.regNo = o.regNo)
    where convert(date,convert(char,o.registDate -5430000)) between'${startDate}' and '${endDtae}'
  and ( o.hn < '8000000'  or o.hn > '9000000' )  
  and o.ipdStatus = '0'
    and not exists(Select pd.Hn from PATDIAG pd where pd.Hn = o.hn and pd.regNo = o.regNo and pd.ICDCode in('Z095')
  and convert(real,b.totalAmt) <= 0  )
    
  
    union all 
    SELECT i.hn+i.regist_flag as seq,RTRIM(LTRIM(i.hn)) as hn
    ,convert(date,convert(char,i.admit_date -5430000)) as date_serv
    ,left(i.admit_time,2)+':'+right(i.admit_time,2) as time_serv
    ,RTRIM(w.ward_name) as department
    from Ipd_h as i
    left join Ward w on w.ward_id = i.ward_id
    where convert(date,convert(char,i.admit_date -5430000)) between'${startDate}' and '${endDtae}'
    `);
    console.log(data);
    
    return data;
  }
  async getServices(db: Knex, hn: any, seq: any) {
    let data = await db.raw(`SELECT ltrim(rtrim(o.hn)) + o.regNo as seq,LTRIM(RTRIM(o.hn)) as hn, convert(date,convert(char,o.registDate -5430000)) as date_serv
    ,left(o.timePt,2)+':'+right (o.timePt,2) as time_serv, RTRIM(d.deptDesc) as department 
    from OPD_H as o 
    left join DEPT d on(d.deptCode  =dbo.Firstcode (o.dept,';')) 
    left join Bill_h b on(b.hn = o.hn and b.regNo = o.regNo)
    where o.hn = dbo.padc('${hn}',' ',7) and o.regNo =  right('${seq}' ,2)
  and ( o.hn < '8000000'  or o.hn > '9000000' )  
  and o.ipdStatus = '0'
    and not exists(Select pd.Hn from PATDIAG pd where pd.Hn = o.hn and pd.regNo = o.regNo and pd.ICDCode in('Z095')
  and convert(real,b.totalAmt) <= 0  )
  
    union all 
    SELECT i.hn+i.regist_flag as seq,RTRIM(LTRIM(hn)) as hn
    ,convert(date,convert(char,i.admit_date -5430000)) as date_serv
    ,left(i.admit_time,2)+':'+right(i.admit_time,2) as time_serv
    ,RTRIM(w.ward_name) as department
    from Ipd_h as i
    left join Ward w on w.ward_id = i.ward_id
    
    where i.hn = dbo.padc('${hn}',' ',7) and i.regist_flag = right('${seq}' ,2)
    `);
    
    
    return data;
  }
  async getDiagnosis(db: Knex, hn: any, dateServe: any, vn: any) {
    let data = await db.raw(`SELECT ltrim(rtrim(p.Hn)) + '-' + p.regNo AS seq
    ,convert(date,convert(char,p.VisitDate -5430000)) AS date_serv
    ,SUBSTRING(o.timePt,1,2)+':'+SUBSTRING(o.timePt,3,4) as time_serv
    ,p.ICDCode as icd_code,ICD101.CODE + '>' + RTRIM(ICD101.DES) AS icd_name
    , p.DiagType AS diag_type,isnull(p.DiagNote,'') as DiagNote 
    ,p.dxtype AS diagtype_id
    FROM PATDIAG p
    left join OPD_H o on(o.hn = p.Hn and o.regNo = p.regNo) 
    LEFT JOIN ICD101 ON p.ICDCode = ICD101.CODE 
    WHERE (p.pt_status in('O','Z')) and p.DiagType in('I','E')
    AND o.hn =dbo.padc('${hn}',' ',7) 
    AND p.regNo = right('${vn}' ,2)
    order by p.VisitDate desc`);
    return data;
  }
  async getProcedure(db: Knex, hn: any, dateServe: any, vn: any) {
    let data = await db.raw(`select ltrim(rtrim(p.hn)) + o.REGNO as seq,
      (o.OR_DATE) as date_serv, o.OR_TIME as time_serv, o.ORCODE as procedure_code, RTRIM(o.ORDESC) as icd_name,
      (o.START_DATE) as start_date, (o.END_DATE) as end_date
    from ORREQ_H o
    left join OPD_H p on(o.HN = p.hn and o.REGNO = p.regNo)
    where o.HN = dbo.padc('${hn}',' ',7) and o.REGNO = right('${vn}', 2) 
    order by date_serv desc `);
    return data;
  }
  async getDrugs(db: Knex, hn: any, dateServe: any, vn: any) {
    let data = await db.raw(` select x.*
    from (select DISTINCT
    ltrim(rtrim(pm.hn)) + pm.registNo as seq
      , convert(date, convert(char, h.registDate - 5430000)) as date_serv
      , SUBSTRING(h.timePt, 1, 2) + ':' + SUBSTRING(h.timePt, 3, 4) as time_serv
      , RTRIM(m.name) as drug_name
      , RTRIM(pm.accAmt) as qty
      , RTRIM(pm.unit) as unit
      , RTRIM(pm.lamedTimeText) + ' ' + RTRIM(pm.lamedText) as usage_line1
      , '' as usage_line2
      , '' as usage_line3
    from Patmed pm(NOLOCK)
    left join OPD_H h(NOLOCK) on(h.hn = pm.hn and  h.regNo = pm.registNo)
    left join Med_inv m(NOLOCK)  on(pm.invCode = m.code and m.site = '1')
    where pm.hn = dbo.padc('${hn}',' ',7)
    and h.ipdStatus = '0'
    AND   pm.registNo = right('${vn}', 2)
    union all
    select DISTINCT
    i.hn + i.regist_flag as seq
      , convert(date, convert(char, i.admit_date - 5430000)) as date_serv
      , left(i.admit_time, 2) + ':' + right(i.admit_time, 2) as time_serv
      , RTRIM(m.name) as drug_name
      , pm.accAmt as qty
      , m.unit as unit
      , RTRIM(pm.lamedTimeText) + ' ' + RTRIM(pm.lamedText) as usage_line1
      , '' as usage_line2
      , '' as usage_line3
    from Patmed pm(NOLOCK)
    left join Ipd_h i(NOLOCK) on(i.hn = pm.hn and  i.regist_flag = pm.registNo)
    left join Med_inv m(NOLOCK)  on(pm.invCode = m.code and m.site = '1')
    where pm.hn = dbo.padc('${hn}',' ',7)  and i.ladmit_n != ''
    AND i.regist_flag = right('${vn}', 2) ) as x 
    order by x.date_serv desc,x.time_serv desc
    `);
    return data;
  }
  async getLabs(db: Knex, hn: any, dateServe: any, vn: any) {
    let data = await db.raw(` 
    select
    convert(date, convert(char, a.res_date - 5430000)) as date_serv
      , left(a.res_time, 2) + ':' + right(a.res_time, 2) as time_serv
      , isnull(RTRIM(a.lab_code), '') as labgroup
      , RTRIM(a.result_name) as lab_name
      , replace(replace(a.real_res,'""',''),'!','') as lab_result
      , s.result_unit as unit
      , a.low_normal + '-' + a.high_normal as standard_result
    from Labres_d a(nolock)
    left join OPD_H o(nolock) on(o.hn = a.hn and o.regNo = a.reg_flag)
    left join Labreq_h b(nolock) on(a.req_no = b.req_no)
    left join Labre_s s(nolock) on(a.lab_code = s.lab_code and a.res_item = s.res_run_no and s.labType = a.lab_type)
    LEFT JOIN Lab lb ON(a.lab_code = lb.labCode and lb.labType = a.lab_type)
    left join Ward w on(w.ward_id = b.reqFrom)
    left join PatSS ss on(ss.hn = a.hn)
    where a.hn = dbo.padc('${hn}',' ',7) AND a.real_res<>''
    AND a.reg_flag = right('${vn}', 2)
    order by a.res_date asc ,left(a.res_time, 2) + ':' + right(a.res_time, 2) desc
    `);
    //console.log(data);

    return data;
  }
  async getNurture(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
    select 
    DISTINCT ltrim(rtrim(v.hn)) + v.RegNo as seq,
    convert(date, convert(char, v.VisitDate)) as date_serv,
    CONVERT(VARCHAR(5),v.VisitDate, 108)  AS time_serv,
    (select  PATIENT.bloodGroup from PATIENT where PATIENT.bloodGroup <> '' and PATIENT.hn = v.hn) as bloodgrp,
    v.Weight AS weight,
    v.Height as height,
    case when v.BMI > '100' then '0' else v.BMI end as bmi,
    v.Temperature AS temperature,
    v.Pulse AS pr,
    v.Breathe AS rr,
    v.Hbloodpress as sbp,
    v.Lbloodpress AS dbp,
    RTRIM(CONVERT(VARCHAR,v.Symtom)) as symptom,
    v.deptCode as depcode,
    (select RTRIM(deptDesc) from DEPT d where d.deptCode = v.deptCode) as department,
    v.GCS_M as movement_score,
    v.GCS_V as vocal_score,
    v.GCS_E as eye_score,
    v.O2sat as oxygen_sat,
    v.pupil_right as pupil_right,
    v.pupil_left as pupil_left,
    v.VitalSignNo,
    CONVERT(INT, v.GCS_M)+CONVERT(INT, v.GCS_V)+CONVERT(INT, v.GCS_E) as gak_coma_sco,
    (select top 1 ltrim(rtrim(dx2.DiagNote)) from PATDIAG dx2 where dx2.Hn = v.hn and dx2.regNo=v.RegNo and dx2.dxtype='1' and dx2.DiagType='I') as diag_text
    
    
    from  PATDIAG dx
    left join VitalSign v on(v.hn=dx.Hn and v.RegNo=dx.regNo)
    where dx.Hn= dbo.padc('${hn}',' ',7) 
    and v.RegNo = right('${seq}', 2)
    order by v.VitalSignNo desc



      `);
      
      
    return data;
  }
  async getPhysical(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
    SELECT ltrim(rtrim(OPD_H.hn)) + PATDIAG.regNo AS seq
    , '' as pe
    FROM OPD_H
    INNER JOIN PATDIAG ON OPD_H.hn = PATDIAG.Hn and OPD_H.regNo = PATDIAG.regNo
    INNER JOIN ICD101 ON PATDIAG.ICDCode = ICD101.CODE
    INNER JOIN DOCC ON PATDIAG.DocCode = DOCC.docCode
    left outer JOIN VitalSign ON OPD_H.hn = VitalSign.hn and OPD_H.regNo = VitalSign.RegNo
    WHERE OPD_H.hn = dbo.padc('${hn}',' ',7) AND PATDIAG.regNo = right('${seq}', 2) AND (PATDIAG.pt_status = 'O')
    order by PATDIAG.regNo desc

      `);
    return data;
  }
  async getPillness(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`SELECT
    ltrim(rtrim(OPD_H.hn)) + PATDIAG.regNo AS seq
      , '' as hpi
    FROM OPD_H
    INNER JOIN PATDIAG ON OPD_H.hn = PATDIAG.Hn and OPD_H.regNo = PATDIAG.regNo
    INNER JOIN ICD101 ON PATDIAG.ICDCode = ICD101.CODE
    INNER JOIN DOCC ON PATDIAG.DocCode = DOCC.docCode
    left outer JOIN VitalSign ON OPD_H.hn = VitalSign.hn and OPD_H.regNo = VitalSign.RegNo
    WHERE OPD_H.hn = dbo.padc('${hn}',' ',7) AND PATDIAG.regNo = right('${seq}', 2) AND (PATDIAG.pt_status = 'O')
    order by PATDIAG.regNo desc

      `);
    return data;
  }
  async getXray(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
          select convert(date, convert(char, x.xreq_date - 5430000)) as xray_date,c.xproc_des as xray_name
          from XresHis x 
          left join OPD_H o (NOLOCK) on (o.hn = x.hn and o.regNo = x.regist_flag) 
          LEFT JOIN Xreq_h xh on(xh.hn = x.hn and xh.regist_flag = x.regist_flag and x.xrunno = xh.xrunno) 
          LEFT JOIN Xpart p ON(x.xpart_code = p.xpart_code) 
          LEFT JOIN Xreq_d d ON(x.xprefix = d.xprefix AND x.xrunno = d.xrunno) 
          LEFT JOIN Xcon xc on(x.xprefix = xc.xprefix) 
          LEFT JOIN Xproc c ON(d.xpart_code = c.xproc_part  AND d.xproc_code = c.xproc_code) 
          WHERE xc.xchr_code <> 'PAT' 
          AND x.hn = dbo.padc('${hn}',' ',7)
          AND x.regist_flag = right('${seq}', 2)
          order by  x.xreq_date DESC
         `);
    return data;
  }
}
