import bcrypt from 'bcrypt';
import fastify from 'fastify';
import path from 'path';
import { join } from 'path'

import autoload from '@fastify/autoload';
require('dotenv').config({ path: join(__dirname, '../config.conf') })

const app = fastify({
  logger: {
    level: process.env.NODE_ENV === 'development' ? 'info' : 'error',
    transport:
      process.env.NODE_ENV === 'development'
        ? {
          target: 'pino-pretty',
          options: {
            translateTime: 'HH:MM:ss Z',
            ignore: 'pid,hostname',
            colorize: true
          }
        }
        : undefined
  }
});

app.register(require('@fastify/formbody'));
app.register(require('@fastify/cors'), {
  origin: ['http://localhost:4200', 'https://r7.moph.go.th'],
  methods: ['GET', 'POST', 'DELETE', 'PUT'],
});

// Rate limit
app.register(import('@fastify/rate-limit'), {
  global: true,
  max: 100,
  timeWindow: '1 minute'
});

// Guard
app.register(
  require('fastify-guard'),
  {
    errorHandler: (_result: any, _request: any, reply: any) => {
      return reply
        .status(405)
        .send({
          ok: false,
          code: 405,
          error: 'You are not allowed to call this route'
        });
    }
  }
);

// Database connection
if(process.env.API_DB_CLIENT == 'mssql'){
  app.register(require('./plugins/db'), {
    options: {
      client: process.env.API_DB_CLIENT,
      connection: {
        host: process.env.API_DB_HOST,
        user: process.env.API_DB_USER,
        port: Number(process.env.API_DB_PORT),
        password: process.env.API_DB_PASSWORD,
        database: process.env.API_DB_NAME,
        requestTimeout: 3000000,
      },
  
      searchPath: [process.env.API_DB_SCHEMA],
      pool: {
  
        min: Number(process.env.API_DB_POOL_MIN),
        max: Number(process.env.API_DB_POOL_MAX),
      },
      debug: process.env.NODE_ENV === "development" ? true : false,
  
    }
  })
}else{
  let set_name = process.env.API_DB_CHARSET || "utf8";
  app.register(require('./plugins/db'), {
    options: {
      client: process.env.API_DB_CLIENT,
      connection: {
        host: process.env.API_DB_HOST,
        user: process.env.API_DB_USER,
        port: Number(process.env.API_DB_PORT),
        password: process.env.API_DB_PASSWORD,
        database: process.env.API_DB_NAME,
        requestTimeout: 3000000,
      },
  
      searchPath: [process.env.API_DB_SCHEMA],
      pool: {
  
        min: Number(process.env.API_DB_POOL_MIN),
        max: Number(process.env.API_DB_POOL_MAX),
        afterCreate: (conn:any, done:any) => {
            conn.query(`SET NAMES ${set_name}`, (err:any) => {
              done(err, conn);
            });
        },
      },
      debug: process.env.NODE_ENV === "development" ? true : false,
  
    }
  })
}


// JWT
app.register(require('@fastify/jwt'), {
  secret: process.env.API_SECRET_KEY,
  sign: {
    iss: 'test.demo.dev',
    expiresIn: '15m'
  },
  messages: {
    badRequestErrorMessage: 'Format is Authorization: Bearer [token]',
    noAuthorizationInHeaderMessage: 'Autorization header is missing!',
    authorizationTokenExpiredMessage: 'Authorization token expired',
    authorizationTokenInvalid: (err: any) => {
      return `Authorization token is invalid: ${err.message}`;
    }
  }
});

app.decorate('hashPassword', async (password: any) => {
  const saltRounds = 12;
  return bcrypt.hash(password, saltRounds);
});

// verify password
app.decorate('verifyPassword', async (password: any, hash: any) => {
  return bcrypt.compare(password, hash);
});

app.register(require('fastify-axios'), {
  baseURL: process.env.API_HIS_ENDPOINT,
});

// routes
app.register(autoload, {
  dir: path.join(__dirname, 'routes'),
  dirNameRoutePrefix: false
});

export default app;
